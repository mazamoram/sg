#include "trajgenerator.h"

trajGenerator::trajGenerator()
{

}

trajGenerator::~trajGenerator()
{

}

double offset1 = 1158.00085449218700;

bool trajGenerator::genGCodeTraj(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                                 QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center,
                                 QList<double> *Radio, QList<double> *Velocity, QList<double> *t, double timeStep, sg mysgp, QList < QList<double>* > jointPosRefmm){

    bool execute = true;
    double tmpTotalTime = 0.0;
    double tmpElapsedTime = 0.0;
    double tmpPercentage = 0.0;

    QListIterator< double > iter1(*t);
    while(iter1.hasNext()){
        tmpTotalTime += iter1.next();
    }

    qDebug()<< "Total time: "<< tmpTotalTime;

    double xi = mysgp.x;
    double yi = mysgp.y;
    double zi = mysgp.z;

    double rolli = mysgp.roll;
    double pitchi = mysgp.pitch;
    double yawi = mysgp.yaw;


    for(int ix = 0; ix < Kind_traj->size(); ix++)
    {
        if (Kind_traj->at(ix) == 1)
        {
            tmpElapsedTime += t->at(ix);
            tmpPercentage = tmpElapsedTime/tmpTotalTime*100.0;
            interpolator inter;

            QList< QList<double> > auxx = inter.interpolate(1, t->at(ix)/1000, timeStep, xi, 0.0, 0.0, X->at(ix), 0,0, 0,0);
            QList< QList<double> > auxy = inter.interpolate(1, t->at(ix)/1000, timeStep, yi, 0.0, 0.0, Y->at(ix), 0,0, 0,0);
            QList< QList<double> > auxz = inter.interpolate(1, t->at(ix)/1000, timeStep, zi, 0.0, 0.0, Z->at(ix), 0,0, 0,0);
            QList< QList<double> > auxquat = inter.interpolate(1, t->at(ix)/1000, timeStep, 0.0, 0.0, 0.0, 1, 0,0, 0,0);

            QQuaternion qi = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), yawi) *
                    QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), pitchi) *
                    QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), rolli) ;

            QQuaternion qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), Yaw->at(ix)) *
                    QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), Pitch->at(ix)) *
                    QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), Roll->at(ix)) ;

            xi = X->at(ix);
            yi = Y->at(ix);
            zi = Z->at(ix);
            rolli = Roll->at(ix);
            pitchi = Pitch->at(ix);
            yawi = Yaw->at(ix);

            QList<QVector3D> Si;
            double auxRoll;
            double auxPitch;
            double auxYaw;
            double q0 = 0;
            double q1 = 0;
            double q2 = 0;
            double q3 = 0;

            for(int i=0; i<auxx.at(0).size(); i++){
                QQuaternion qInterp = QQuaternion::slerp(qi, qf, auxquat.at(0).at(i));
                q0 = qInterp.scalar();
                q1 = qInterp.vector().x();
                q2 = qInterp.vector().y();
                q3 = qInterp.vector().z();
                auxRoll = qAtan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2))*180/M_PI;
                auxPitch = qAsin(2*(q0*q2-q3*q1))*180/M_PI;
                auxYaw = qAtan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3))*180/M_PI;

                Si = mysgp.invk(QVector3D (0*mysgp.x + auxx.at(0).at(i),
                                           0*mysgp.y + auxy.at(0).at(i),
                                           0*mysgp.z + auxz.at(0).at(i)),
                                auxRoll,
                                auxPitch,
                                auxYaw);
                //qDebug()<< auxx.at(0).at(i) << ", "<< auxy.at(0).at(i) << ", "<< auxz.at(0).at(i);
                if(execute){execute = 0.01 < qFabs(det(mysgp.jacob(Si))); }
                //qDebug()<<qFabs(det(mysgp.jacob(Si)));
                for (int j = 0; j < 6; j++){
                    jointPosRefmm.at(j)->append(Si.at(j).length() - offset1);
                    if (execute){ execute = (jointPosRefmm.at(j)->last() > 0  && jointPosRefmm.at(j)->last() < 450 ); }
                }
            }

        }else if (Kind_traj->at(ix) == 3 || Kind_traj->at(ix) == 2)
        {
            QVector3D pi = QVector3D(xi,yi,zi);
            QVector3D pf = QVector3D(X->at(ix), Y->at(ix), Z->at(ix));
            QVector3D pc = QVector3D(X_center->at(ix), Y_center->at(ix), Z_center->at(ix));
            QVector3D di = pi-pc;
            QVector3D df = pf-pc;

            QVector3D nv = QVector3D::crossProduct(di,df);
            double dotp = QVector3D::dotProduct(di,df);
            double theta = qAcos(dotp/(di.length()*df.length()))*180/M_PI;

            interpolator inter;
            QList< QList<double> > auxTheta = inter.interpolate(1, t->at(ix)/1000, timeStep, 0.0, 0.0, 0.0, theta, 0,0, 0,0);
            QList< QList<double> > auxquat = inter.interpolate(1, t->at(ix)/1000, timeStep, 0.0, 0.0, 0.0, 1, 0,0, 0,0);

            QQuaternion qi = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), yawi) *
                    QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), pitchi) *
                    QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), rolli) ;

            QQuaternion qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), Yaw->at(ix)) *
                    QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), Pitch->at(ix)) *
                    QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), Roll->at(ix)) ;

            xi = X->at(ix);
            yi = Y->at(ix);
            zi = Z->at(ix);
            rolli = Roll->at(ix);
            pitchi = Pitch->at(ix);
            yawi = Yaw->at(ix);

            QList<QVector3D> Si;
            double auxRoll;
            double auxPitch;
            double auxYaw;
            double q0 = 0;
            double q1 = 0;
            double q2 = 0;
            double q3 = 0;
            QMatrix4x4 Rv;
            QVector3D p;
            for(int i=0; i<auxTheta.at(0).size(); i++){
                QQuaternion qInterp = QQuaternion::slerp(qi, qf, auxquat.at(0).at(i));
                q0 = qInterp.scalar();
                q1 = qInterp.vector().x();
                q2 = qInterp.vector().y();
                q3 = qInterp.vector().z();
                auxRoll = qAtan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2))*180/M_PI;
                auxPitch = qAsin(2*(q0*q2-q3*q1))*180/M_PI;
                auxYaw = qAtan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3))*180/M_PI;
                Rv.setToIdentity();
                Rv.rotate(QQuaternion::fromAxisAndAngle(nv, auxTheta.at(0).at(i)));
                p =  pc + Rv*di;
                Si = mysgp.invk(p, auxRoll, auxPitch, auxYaw);

                if(execute){execute = 0.01 < qFabs(det(mysgp.jacob(Si))); }
                //-* qDebug()<<qFabs(det(mysgp.jacob(Si)));
                for (int j = 0; j < 6; j++){
                    jointPosRefmm.at(j)->append(Si.at(j).length() - offset1);
                    if (execute){ execute = (jointPosRefmm.at(j)->last() > 0  && jointPosRefmm.at(j)->last() < 450 ); }
                }
            }

            Rv.setToIdentity();
            Rv.rotate(QQuaternion::fromAxisAndAngle(nv, theta));
            QVector3D errSi = (Rv*di-df);
            //qDebug()<<"errSi"<< errSi;
            if (errSi.length()<0.16){
                //qDebug()<<"Rigth theta";
            }else{
                qDebug()<<"False theta: "<< errSi.length();
            }
        }
    }
    return execute;
}


void trajGenerator::nAxisMove(int axisId, double direction, double jointStep, int pot_dos,
                              int interpolationType, double timeInterval, double maxJerk,
                              QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                              QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm)
{

    qDebug()<<"From the trajGen thread: "<<QThread::currentThreadId();
    double timeStep = pow(2,pot_dos)/1000.0;
    interpolator inter;

    QList< QList<double> > aux = inter.interpolate(interpolationType, timeInterval/1000.0,
                                                   timeStep, 0.0, 0.0, 0.0, direction*jointStep, 0, 0,
                                                   timeInterval, maxJerk);

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    for(int i=0; i<aux.at(0).size(); i++){
        jointPosRefmm.at(axisId-1)->append(aux.at(0).at(i));
        //jointVelNumRefmm.at(axisId-1)->append(aux.at(1).at(i));
    }
    emit execManualJointSignal(axisId, aux, timeStep);
}

void trajGenerator::nAxisMoveN(int axisId, int nTimes, double jointStep, int pot_dos,
                               int interpolationType, double timeInterval, double maxJerk,
                               QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                               QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm){

    double timeStep = pow(2,pot_dos)/1000.0;
    interpolator inter;
    QList< QList<double> > aux;
    QList<double> auxPos;
    QList<double> auxVel;
    QList<double> auxAcc;
    QList<double> auxJerk;
    QList< QList<double> > aux2;

    for (int i=0; i <nTimes; i++)
    {
        aux2 = inter.interpolate(interpolationType, timeInterval/1000.0, timeStep, 0.0, 0.0, 0.0, jointStep, 0,0,
                                 timeInterval, maxJerk);

        for (int j=0; j< aux2.at(0).size(); j++)
        {
            auxPos.append(aux2.at(0).at(j));
            auxVel.append(aux2.at(1).at(j));
            auxAcc.append(aux2.at(2).at(j));
            auxJerk.append(aux2.at(3).at(j));
        }

        aux2.clear();
        aux2 = inter.interpolate(interpolationType, timeInterval/1000.0,
                                 timeStep, jointStep, 0.0, 0.0, 0.0, 0,0,
                                 timeInterval, maxJerk);
        for (int j=0; j< aux2.at(0).size(); j++)
        {
            auxPos.append(aux2.at(0).at(j));
            auxVel.append(aux2.at(1).at(j));
            auxAcc.append(aux2.at(2).at(j));
            auxJerk.append(aux2.at(3).at(j));
        }
    }

    aux.append(auxPos);
    aux.append(auxVel);
    aux.append(auxAcc);
    aux.append(auxJerk);

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    for(int i=0; i<aux.at(0).size(); i++){
        jointPosRefmm.at(axisId-1)->append(aux.at(0).at(i));
    }

    emit execManualJointSignal(axisId, aux, timeStep);
}


void trajGenerator::moveWSpace(QString wsAxisId, double direction, double wSpaceStep, int pot_dos,
                               int interpolationType, double timeInterval, double maxJerk,
                               QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                               QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm, sg mysgp){

    double timeStep = pow(2,pot_dos)/1000.0;

    interpolator inter;
    QList< QList<double> > aux;
    aux = inter.interpolate(interpolationType, timeInterval/1000.0,
                            timeStep, 0.0, 0.0, 0.0, direction*wSpaceStep, 0,0,
                            timeInterval, maxJerk);

    QList<QVector3D> Si;

    bool execute = true;

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    for(int i=0; i<aux.at(0).size(); i++){
        if(wsAxisId == "X")
            Si = mysgp.invk(QVector3D (mysgp.x + aux.at(0).at(i),  mysgp.y, mysgp.z ), mysgp.roll,mysgp.pitch,mysgp.yaw);
        else if(wsAxisId == "Y")
            Si = mysgp.invk(QVector3D (mysgp.x,  mysgp.y + aux.at(0).at(i), mysgp.z ), mysgp.roll,mysgp.pitch,mysgp.yaw);
        else if(wsAxisId == "Z")
            Si = mysgp.invk(QVector3D (mysgp.x,  mysgp.y, mysgp.z + aux.at(0).at(i) ), mysgp.roll,mysgp.pitch,mysgp.yaw);

        for (int j = 0; j < 6; j++){
            jointPosRefmm.at(j)->append(Si.at(j).length() - offset);
            if (execute){ execute = (jointPosRefmm.at(j)->last() > 0); }
        }
    }

    emit execManualWSpaceSignal(wsAxisId, direction, wSpaceStep, execute, timeStep, false);
}


void trajGenerator::moveWSpaceN(QString wsAxisId, int nTimes, double wSpaceStep, int pot_dos,
                                int interpolationType, double timeInterval, double maxJerk,
                                QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                                QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm, sg mysgp){

    double timeStep = pow(2,pot_dos)/1000.0;

    interpolator inter;
    QList< QList<double> > aux;
    QList< QList<double> > aux2;
    QList<double> auxPos;
    QList<double> auxVel;
    QList<double> auxAcc;
    QList<double> auxJerk;

    for (int i=0; i <nTimes; i++)
    {
        aux2 = inter.interpolate(interpolationType, timeInterval/1000.0,
                                 timeStep, 0.0, 0.0, 0.0, wSpaceStep, 0,0,
                                 timeInterval, maxJerk);
        for (int j=0; j< aux2.at(0).size(); j++)
        {
            auxPos.append(aux2.at(0).at(j));
            auxVel.append(aux2.at(1).at(j));
            auxAcc.append(aux2.at(2).at(j));
            auxJerk.append(aux2.at(3).at(j));
        }

        aux2.clear();
        aux2 = inter.interpolate(interpolationType, timeInterval/1000.0,
                                 timeStep, wSpaceStep, 0.0, 0.0, 0.0, 0,0,
                                 timeInterval, maxJerk);
        for (int j=0; j< aux2.at(0).size(); j++)
        {
            auxPos.append(aux2.at(0).at(j));
            auxVel.append(aux2.at(1).at(j));
            auxAcc.append(aux2.at(2).at(j));
            auxJerk.append(aux2.at(3).at(j));
        }
    }

    aux.append(auxPos);
    aux.append(auxVel);
    aux.append(auxAcc);
    aux.append(auxJerk);

    double tAux = 0;
    QList<QVector3D> Si;

    bool execute = true;

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    for(int i=0; i<aux.at(0).size(); i++){

        if(wsAxisId == "X")
            Si = mysgp.invk(QVector3D (mysgp.x + aux.at(0).at(i),  mysgp.y, mysgp.z ), mysgp.roll,mysgp.pitch,mysgp.yaw);
        else if(wsAxisId == "Y")
            Si = mysgp.invk(QVector3D (mysgp.x,  mysgp.y + aux.at(0).at(i), mysgp.z ), mysgp.roll,mysgp.pitch,mysgp.yaw);
        else if(wsAxisId == "Z")
            Si = mysgp.invk(QVector3D (mysgp.x,  mysgp.y, mysgp.z + aux.at(0).at(i) ), mysgp.roll,mysgp.pitch,mysgp.yaw);

        for (int j = 0; j < 6; j++){
            jointPosRefmm.at(j)->append(Si.at(j).length() - offset);
            if (execute){ execute = (jointPosRefmm.at(j)->last() > 0); }
        }

        tAux += timeStep*1000;
    }

    emit execManualWSpaceSignal(wsAxisId, 1, wSpaceStep, execute, timeStep, true);
}

void trajGenerator::rotWSpace(QString rotAxisId, double direction, double angleStep, int pot_dos,
                              int interpolationType, double timeInterval, double maxJerk,
                              QList < QList<double>* > jointPosRefmm, QList < QList<double>* > jointVelNumRefmm,
                              QList < QList<double>* > jointAccNumRefmm, QList < QList<double>* > jointJerkNumRefmm,
                               sg mysgp ){

    double timeStep = pow(2,pot_dos)/1000.0;

    interpolator inter;
    QList< QList<double> > aux;
    aux = inter.interpolate(interpolationType, timeInterval/1000.0, timeStep, 0.0, 0.0, 0.0, 1, 0,0,
                            timeInterval, maxJerk);

    QQuaternion qi = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw) *
            QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch) *
            QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll) ;

    QQuaternion qf;

    if(rotAxisId == "R"){
        qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw) *
                QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch) *
                QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll+direction*angleStep) ;
    }else if(rotAxisId == "P"){
        qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw) *
                QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch+direction*angleStep) *
                QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll) ;
    }else if(rotAxisId == "YZ"){
        qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw+direction*angleStep) *
                QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch) *
                QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll) ;
    }

    QQuaternion qInterp;

    QList<QVector3D> Si;
    bool execute = true;
    double auxRoll = 0;
    double auxPitch = 0;
    double auxYaw = 0;
    double q0 = 0;
    double q1 = 0;
    double q2 = 0;
    double q3 = 0;

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    for(int i=0; i<aux.at(0).size(); i++){

        qInterp = QQuaternion::slerp(qi, qf, aux.at(0).at(i));
        q0 = qInterp.scalar();
        q1 = qInterp.vector().x();
        q2 = qInterp.vector().y();
        q3 = qInterp.vector().z();
        auxRoll = qAtan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2))*180/M_PI;
        auxPitch = qAsin(2*(q0*q2-q3*q1))*180/M_PI;
        auxYaw = qAtan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3))*180/M_PI;

        Si = mysgp.invk(QVector3D (mysgp.x ,  mysgp.y, mysgp.z ), auxRoll, auxPitch, auxYaw);

        for (int j = 0; j < 6; j++){
            jointPosRefmm.at(j)->append(Si.at(j).length() - offset);
            if (execute){ execute = (jointPosRefmm.at(j)->last() > 0); }
        }
    }

    emit execManualWSpaceSignal(rotAxisId, direction, angleStep, execute, timeStep, false);
}

void trajGenerator::rotWSpaceN(QString rotAxisId, int nTimes, double angleStep, int pot_dos,
                               int interpolationType, double timeInterval, double maxJerk,
                               QList < QList<double>* > jointPosRefmm, QList < QList<double>* > jointVelNumRefmm,
                               QList < QList<double>* > jointAccNumRefmm, QList < QList<double>* > jointJerkNumRefmm,
                               sg mysgp ){

    double timeStep = pow(2,pot_dos)/1000.0;

    interpolator inter;
    QList< QList<double> > aux;
    QList< QList<double> > aux2;
    QList<double> auxPos;
    QList<double> auxVel;
    QList<double> auxAcc;
    QList<double> auxJerk;

    for (int i=0; i <nTimes; i++)
    {
        aux2 = inter.interpolate(interpolationType, timeInterval/1000.0, timeStep, 0.0, 0.0, 0.0, 1.0, 0,0,
                                 timeInterval, maxJerk);
        for (int j=0; j< aux2.at(0).size(); j++){
            auxPos.append(aux2.at(0).at(j));
            auxVel.append(aux2.at(1).at(j));
            auxAcc.append(aux2.at(2).at(j));
            auxJerk.append(aux2.at(3).at(j));
        }

        aux2.clear();
        aux2 = inter.interpolate(interpolationType, timeInterval/1000.0, timeStep, 1.0, 0.0, 0.0, 0.0, 0,0,
                                 timeInterval, maxJerk);
        for (int j=0; j< aux2.at(0).size(); j++){
            auxPos.append(aux2.at(0).at(j));
            auxVel.append(aux2.at(1).at(j));
            auxAcc.append(aux2.at(2).at(j));
            auxJerk.append(aux2.at(3).at(j));
        }
    }

    aux.append(auxPos);
    aux.append(auxVel);
    aux.append(auxAcc);
    aux.append(auxJerk);

    QQuaternion qi = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw) *
            QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch) *
            QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll) ;

    QQuaternion qf;

    if(rotAxisId == "R")
    {
        qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw) *
                QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch) *
                QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll+1*angleStep) ;
    }else if(rotAxisId == "P")
    {
        qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw) *
                QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch+1*angleStep) *
                QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll) ;
    }else if(rotAxisId == "YZ")
    {
        qf = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), mysgp.yaw+1*angleStep) *
                QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), mysgp.pitch) *
                QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), mysgp.roll) ;
    }

    QQuaternion qInterp;

    QList<QVector3D> Si;
    bool execute = true;
    double auxRoll = 0;
    double auxPitch = 0;
    double auxYaw = 0;
    double q0 = 0;
    double q1 = 0;
    double q2 = 0;
    double q3 = 0;

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    for(int i=0; i<aux.at(0).size(); i++){

        qInterp = QQuaternion::slerp(qi, qf, aux.at(0).at(i));
        q0 = qInterp.scalar();
        q1 = qInterp.vector().x();
        q2 = qInterp.vector().y();
        q3 = qInterp.vector().z();
        auxRoll = qAtan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2))*180/M_PI;
        auxPitch = qAsin(2*(q0*q2-q3*q1))*180/M_PI;
        auxYaw = qAtan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3))*180/M_PI;

        Si = mysgp.invk(QVector3D (mysgp.x ,  mysgp.y, mysgp.z ), auxRoll, auxPitch, auxYaw);


        for (int j = 0; j < 6; j++){
            jointPosRefmm.at(j)->append(Si.at(j).length() - offset);
            if (execute){ execute = (jointPosRefmm.at(j)->last() > 0); }
        }
    }

    emit execManualWSpaceSignal(rotAxisId, 1, angleStep, execute, timeStep, true);
}
