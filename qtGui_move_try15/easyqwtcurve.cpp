#include "easyqwtcurve.h"

easyQwtCurve::easyQwtCurve()
{
    curve = new QwtPlotCurve();
    curve->setRenderHint(QwtPlotItem:: RenderAntialiased, true);//smoothing
    curveSmpls = new QVector<QPointF>;
    colorRef = QColor(Qt::black);
}

easyQwtCurve::~easyQwtCurve()
{

}

void easyQwtCurve::setQwtPlot(QwtPlot* qwtPlotRef){
    this->qwtPlotRef = qwtPlotRef;
    curve->attach(qwtPlotRef);
}

void easyQwtCurve::setColor(QColor color){
    colorRef =  color;
}

void easyQwtCurve::set(QwtPlot* qwtPlotRef, QColor color, int lineWidth, Qt::PenStyle S)
{
    /*
    this->setQwtPlot(qwtPlotRef);
    this->setColor(color);
    */

    this->qwtPlotRef = qwtPlotRef;
    curve->attach(qwtPlotRef);
    colorRef =  color;
    curve->setPen(* new QPen(colorRef, lineWidth, S));
}

void easyQwtCurve::plot(QList <double> ydata)
{
    curveSmpls->clear();

    for(int i = 0; i<ydata.size(); i++){
        curveSmpls->push_back(QPointF(i, ydata.at(i)));
    }
    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

void easyQwtCurve::updatePlot(QList <double> ydata)
{
    //curveSmpls->clear();

    int ix = curveSmpls->length();
    if (ix == 0) {ix = 1;}

    for(int i = ix-1; i<ydata.size(); i++){
        curveSmpls->push_back(QPointF(i, ydata.at(i)));
    }

    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

//void easyQwtCurve::plot(double timeStep, QList <double> ydata){
//    curveSmpls->clear();
//    double auxTime = 0;
//    for(int i = 0; i<ydata.size(); i++){
//        curveSmpls->push_back(QPointF(auxTime, ydata.at(i)));
//        auxTime += timeStep;
//    }
//    curve->setSamples(*curveSmpls);
//    //curve->setPen(* new QPen(colorRef));
//    //--curve->attach(qwtPlotRef);
//    qwtPlotRef->replot();
//}

//void easyQwtCurve::updatePlot(double timeStep, QList <double> ydata)
//{
//    //curveSmpls->clear();
//    int ix = curveSmpls->length();
//    if (ix == 0) {ix = 1;}
//    double auxTime = timeStep * curveSmpls->length();

//    for(int i = ix-1; i<ydata.size(); i++){
//        curveSmpls->push_back(QPointF(auxTime, ydata.at(i)));
//    }
//    curve->setSamples(*curveSmpls);
//    //curve->setPen(* new QPen(colorRef));
//    //--curve->attach(qwtPlotRef);
//    qwtPlotRef->replot();
//}

void easyQwtCurve::plot(QList <double> xdata, QList <double> ydata, double xbias, double ybias, double xScale, double yScale){
    curveSmpls->clear();

    for(int i = 0; i<xdata.size() && i<ydata.size(); i++){
        curveSmpls->push_back(QPointF((xdata.at(i)-xbias)*xScale, (ydata.at(i)-ybias)*yScale));
    }
    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

void easyQwtCurve::updatePlot(QList <double> xdata, QList <double> ydata, double xbias, double ybias, double xScale, double yScale){
    //curveSmpls->clear();
    int ix = curveSmpls->length();
    if (ix == 0) {ix = 1;}

    for(int i = ix-1; i<xdata.size() && i<ydata.size(); i++){
        curveSmpls->push_back(QPointF((xdata.at(i)-xbias)*xScale, (ydata.at(i)-ybias)*yScale));
    }
    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

void easyQwtCurve::plot(double timeStep, QList <double> ydata, double xbias, double ybias, double xScale, double yScale){
    curveSmpls->clear();
    double auxTime = 0;
    for(int i = 0; i<ydata.size(); i++){
        curveSmpls->push_back(QPointF((auxTime-xbias)*xScale, (ydata.at(i)-ybias)*yScale));
        auxTime += timeStep;
    }
    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

void easyQwtCurve::updatePlot(double timeStep, QList <double> ydata, double xbias, double ybias, double xScale, double yScale){
    //curveSmpls->clear();
    int ix = curveSmpls->length();
    if (ix == 0) {ix = 1;}
    double auxTime = 0;
    for(int i = ix -1; i<ydata.size(); i++){
        curveSmpls->push_back(QPointF((auxTime-xbias)*xScale, (ydata.at(i)-ybias)*yScale));
        auxTime += timeStep;
    }
    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}


void easyQwtCurve::hline(double y, double ti, double tf){
    curveSmpls->clear();
    curveSmpls->push_back(QPointF(ti, y));
    curveSmpls->push_back(QPointF(tf, y));
    curve->setSamples(*curveSmpls);
    //curve->setPen(* new QPen(colorRef));
    //--curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

void easyQwtCurve::attach(){
    curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}

void easyQwtCurve::dettach(){
    curve->detach();
    qwtPlotRef->replot();
}

void easyQwtCurve::clear(){
    curveSmpls->clear();
    curve->setSamples(*curveSmpls);
    //curve->attach(qwtPlotRef);
    qwtPlotRef->replot();
}
