#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H

#include <QList>

class interpolator
{
public:

    interpolator();
    ~interpolator();
    QList< QList<double> > interpolate(int interpolatorType, double tf, double step, double qi, double qpi, double qppi,
                              double qf, double qpf, double qppf, double vmax, double jerkmax);
    QList< QList<double> > quinticInterp(double tf, double step, double qi, double qpi, double qppi,
                                      double qf, double qpf, double qppf);
    QList< QList<double> > sInterp(double step, double vmax, double jmax, double pi, double vi, double ai,
                                      double pf, double vf, double af);


};


#endif // INTERPOLATOR_H
