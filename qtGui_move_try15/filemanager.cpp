#include "filemanager.h"
#include "conversions.h"

fileManager::fileManager()
{

}

fileManager::~fileManager()
{

}


void fileManager::saveRef(QList < QList<double>* > jointPosRefmm, QString fName){
    if(this->logPath != "")
    {
        this->refData.open(this->logPath.toUtf8()  + "/" +  fName.toUtf8() );

        for (int j = 0; j < jointPosRefmm.at(0)->size(); j++){
            for(int i=0; i < jointPosRefmm.size()-1; i++){
                this->refData <<jointPosRefmm.at(i)->at(j) << ",";
            }
            this->refData <<jointPosRefmm.at( jointPosRefmm.size()-1)->at(j);
            this->refData << "\n";
        }
        this->refData.close();
    }
}

void fileManager::saveSensor(QList<QList<double> *> gPos, QList<QList<double> *> gVel, QList<QList<double> *> gErr,
                             QList<QList<double> *> gTorque, QList<double> TimeC, QString fName){

    if(this->logPath != "")
    {
        this->sensorData.open(this->logPath.toUtf8() +"/" + fName.toUtf8() );

        for (int j = 0; j < gPos.at(0)->size(); j++){
            for(int i=0; i < gPos.size(); i++){
                this->sensorData <<gPos.at(i)->at(j)*pulses2mmFactor << ",";
            }
            for(int i=0; i < gVel.size(); i++){
                this->sensorData <<gVel.at(i)->at(j)*pulses2mmFactor << ",";
            }
            for(int i=0; i < gErr.size(); i++){
                this->sensorData <<gErr.at(i)->at(j)*pulses2mmFactor << ",";
            }
            for(int i=0; i < gTorque.size(); i++){
                this->sensorData <<gTorque.at(i)->at(j) << ",";
            }
            this->sensorData <<TimeC.at(j) << "\n";
        }
        this->sensorData.close();
    }
}


void fileManager::saveLogDialog(QList<QList<double> *> gPos, QList<QList<double> *> gVel, QList<QList<double> *> gPosErr,
                                QList<QList<double> *> gTorque, QList<double> TimeC, QList<QList<double> *> jointPosRefmm)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Message");
    msgBox.setText("Do you want to save the last move?");
    msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    if(msgBox.exec() == QMessageBox::Yes){

        this->logPath = QFileDialog::getExistingDirectory(0,"Selecte folder to save files");
        QString fPostfix =  QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm");

        this->saveSensor(gPos, gVel, gPosErr,gTorque, TimeC, "sensor_" + fPostfix+ ".log" );
        this->saveRef(jointPosRefmm, "ref_" + fPostfix+ ".log");

        if (this->logPath != ""){
            QMessageBox msgBox1;
            msgBox1.setWindowTitle("Message");
            msgBox1.setText("Trajectories have been saved under: ref_" + fPostfix+ ".log and sensor_" + fPostfix+ ".log" );
            msgBox1.setStandardButtons(QMessageBox::Ok);
            msgBox1.setDefaultButton(QMessageBox::Ok);
            msgBox1.exec();
        }
    }
}

bool fileManager::loadTrajectory( QList< QList<double> > *pos_mm, QList< QList<double> > *vel_mmps,
                                  QList< QList<double> > *acc_mmps2, QList< double > *time_s )
{

    bool executeTraj =  true;
    QString fileName =
            QFileDialog::getOpenFileName(0,"Open Trajectory", QDir::currentPath(), "dat (*.dat)");

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly)) {

        // Checking reading time
        this->timer.start();

        // Deleting previos trajectory
        clnr.clearJointTrajectory( pos_mm, vel_mmps, acc_mmps2, time_s );

        // Reading file line by line.
        QTextStream in(&file);
        while (!in.atEnd()){
            QList<QString> aux = (in.readLine() ).split(",");
            QList<double> auxpos;
            QList<double> auxvel;
            QList<double> auxacc;

            for(int i=0; i<=17; i++)
            {
                int res = i%3;
                if(res == 0){
                    auxpos<< aux.at(i).toDouble();
                }
                else if(res == 1){
                    auxvel<< aux.at(i).toDouble();
                }
                else if(res == 2){
                    auxacc<< aux.at(i).toDouble();
                }
            }
            pos_mm->append( auxpos );
            for(int i=0; i<6; i++)
            {
                if(executeTraj){
                    double diff = (pos_mm->last().at(i) - pos_mm->at(0).at(i));
                    executeTraj =  diff >= 0 && diff <= 450;
                }
            }
            vel_mmps->append( auxvel );
            acc_mmps2->append( auxacc );
            time_s->append( aux.at(18).toDouble() );
        }
        qDebug() << "The slow operation took1" << this->timer.elapsed() << "milliseconds";
    }
    return executeTraj;
}



bool fileManager::loadGCode(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                               QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center,
                               QList<double> *Radio, QList<double> *Velocity, QList<double> *t)
{

    bool gCodeLoaded = false;
    //    QString fileName =
    //            QFileDialog::getOpenFileName(0,"Open Trajectory", QDir::currentPath(), "dat (*.dat)");

    //    QFile file(fileName);
    //    if(file.open(QIODevice::ReadOnly)


    QString fileName =
            QFileDialog::getOpenFileName(0, "Open G Code", QDir::currentPath(), "txt (*.txt)");

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly)) {

        clnr.clearGCodeTrajectory( Kind_traj, Roll, Pitch, Yaw, X, Y, Z, X_center, Y_center, Z_center,
                                   Radio,  Velocity, t);

        // Reading file line by line.
        QTextStream in(&file);
        while (!in.atEnd()){
            QList<QString> aux = (in.readLine() ).split("\t");
            //qDebug() << "lodeGCode: "<< aux;
            int gCode = aux.at(0).toInt();
            if (gCode == 1)
            {
                if (aux.size() < 9)
                {
                    qDebug()<< "Wrong line";
                }
                else{
                    Kind_traj->append( gCode );
                    X->append( aux.at(1).toDouble() );
                    Y->append( aux.at(2).toDouble() );
                    Z->append( aux.at(3).toDouble() );
                    Roll->append( aux.at(4).toDouble() );
                    Pitch->append( aux.at(5).toDouble() );
                    Yaw->append( aux.at(6).toDouble() );
                    Velocity->append( aux.at(7).toDouble() );
                    t->append( aux.at(8).toDouble());
                    X_center->append( 0.0 );
                    Y_center->append( 0.0 );
                    Z_center->append( 0.0 );

                    Radio->append( 0.0 );
                }
            }else if (gCode == 2 || gCode == 3)
            {
                if (aux.size() < 12)
                {
                    qDebug()<< "Wrong line";
                }
                else{
                    Kind_traj->append( gCode );
                    X->append( aux.at(1).toDouble() );
                    Y->append( aux.at(2).toDouble() );
                    Z->append( aux.at(3).toDouble() );
                    X_center->append( aux.at(4).toDouble() *1);
                    Y_center->append( aux.at(5).toDouble() *1);
                    Z_center->append( aux.at(6).toDouble() *1);
                    Roll->append( aux.at(7).toDouble()*0);
                    Pitch->append( aux.at(8).toDouble() *0);
                    Yaw->append( aux.at(9).toDouble() *0);
                    Velocity->append( aux.at(10).toDouble() );
                    t->append( aux.at(11).toDouble() );

                    Radio->append( 0.0 );

                }
            }
        }
        gCodeLoaded = true;
    }

    return gCodeLoaded;
}


void fileManager::readParameters(RobotParameters *rp)
{
    QFile file("/home/mike/Documentos/2015/StewartGough/QtProjects/projects/qtGui_move_try15/ConfigFiles/SG.par");
    if(file.open(QIODevice::ReadOnly)) {

        // Reading file line by line.
        QTextStream in(&file);
        while (!in.atEnd()){
            QList<QString> aux = (in.readLine() ).split(",");
            if(aux.length() == 6){
                rp->diaBase = aux.at(0).toDouble();
                rp->A1 = aux.at(1).toDouble();
                rp->diaMobile = aux.at(2).toDouble();
                rp->A2 = aux.at(3).toDouble();
                rp->L1 = aux.at(4).toDouble();
                rp->L2 = aux.at(5).toDouble();
                qDebug()<< rp->diaBase;
            }
        }
    }
}
