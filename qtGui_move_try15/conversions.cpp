#include <conversions.h>

QList< QList<double> > mm2pulses(QList< QList<double> > points_mm)
{
    QList< QList<double> > points_pulses;
    QListIterator< QList<double> > iter1(points_mm);
    while(iter1.hasNext()){
        QListIterator< double > iter2(iter1.next());
        QList<double> auxDouble;
        while(iter2.hasNext()){
            double aux1 = iter2.next();
            auxDouble << (double) aux1* double(mm2pulsesFactor);
        }
        points_pulses << auxDouble;
    }
    return points_pulses;
}

double mm2pulses(double point_mm)
{
    return point_mm*mm2pulsesFactor;
}

double pulses2mm (double point_pulses)
{
    return point_pulses*pulses2mmFactor;
}

