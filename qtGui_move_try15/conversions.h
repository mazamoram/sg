#ifndef CONVERSIONS
#define CONVERSIONS
#include <QList>
const double mm2pulsesFactor = 8192*4.0/5.0; // Numero de pulsos por vuelta/Numero de pulsos por mm
const double pulses2mmFactor = 1/mm2pulsesFactor;

QList< QList<double> > mm2pulses(QList< QList<double> > points_mm);
double mm2pulses(double point_mm);
double pulses2mm (double point_pulses);

#endif // CONVERSIONS

