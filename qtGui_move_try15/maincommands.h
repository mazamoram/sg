#ifndef MAINCOMMANDS
#define MAINCOMMANDS
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QElapsedTimer>
#include <QList>
#include <QListIterator>
#include <Galil.h>



void dispGCodeInfo(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center, QList<double> *Radio,
                QList<double> *Velocity, QList<double> *t );

void servoOff(Galil *g);
void servoOn(Galil *g);

void numericDeriv(QList < QList<double>* > p, QList<QList<double> *> *v, double dt);
void numericDeriv2(QList < QList<double>* > p, QList < QList<double>* > *a, double dt);

#endif // MAINCOMMANDS

