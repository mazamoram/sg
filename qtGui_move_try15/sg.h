#ifndef SG_H
#define SG_H
#include <QList>
#include <qvector3d.h>
#include <qmatrix4x4.h>
#include <iostream>
#include <QDebug>
#include <math.h>
#include <QtMath>
#include "armadillo"

class sg
{
public:
    double A1;
    double RadBase;
    double L1;
    double A2;
    double RadMobile;
    double L2;
    QList<QVector3D> ai;
    QList<QVector3D> bi;

    double x, y, z, roll, pitch, yaw;

    sg();
    sg(double A1, double RadBase, double L1, double A2, double RadMobile, double L2);
    ~sg();
    QList<QVector3D> invk(QVector3D p, double roll, double pitch, double yaw);

    void setConfiguration( double RadBase, double A1, double RadMobile, double A2,  double L1, double L2);
    QList<double> getConfiguration();
    void setPose(float x, float y, float z, float roll, float pitch, float yaw);
    QList<double> getPose();
    //QList<double> jacob();
    arma::mat jacob( QList<QVector3D> Si );


};

#endif // SG_H

