#ifndef ROBOTCONTROLLER_H
#define ROBOTCONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QElapsedTimer>
#include <QString>
#include <QList>
#include <QThread>

#include <Galil.h>
#include <conversions.h>

class RobotController: public QObject
{
    Q_OBJECT

public:
    RobotController();
    ~RobotController();

    bool motionEnable;
    int cntr, weirdCntr, axisId, axisIdAux, contador, Bloque, Buffer, lastTime;

    QString CD;
    QList<double> TimeC;

    QList<QList<double> *> gPos;
    QList<double> gPosA, gPosB, gPosC, gPosD, gPosE, gPosF;

    QList<QList<double> *> gPosErr;
    QList<double> gPosErrA, gPosErrB, gPosErrC, gPosErrD, gPosErrE, gPosErrF;

    QList<QList<double> *> gVel;
    QList<double> gVelA, gVelB, gVelC, gVelD, gVelE, gVelF;

    QList<QList<double> *> gTorque;
    QList<double> gTorqueA, gTorqueB, gTorqueC, gTorqueD, gTorqueE, gTorqueF;

    QList<QString> TP, TE, TV, TT;
    QElapsedTimer myTimer;

    QString createCD(int axisId, QList <QList<double>* > jointPosRefmm, int i);

    void clearMeasurements();

    void readSensors(Galil *g);

    void sendBlock(Galil *g, int lowerIx, int upperIx, QList < QList<double>* > jointPosRefmm,
                   int axisId, QString p);

    void jMoveLoopSlot(Galil *g, QList < QList<double>* > jointPosRefmm);

public slots:
    void jMoveSlot(Galil *g,  int pot_dos, int axisId, QList < QList<double>* > jointPosRefmm);

signals:
    void updatePlotSignal(bool absMotion, QList<double> TimeC, QList<QList<double> *> gPos,
                          QList<QList<double> *> gPosErr, QList<QList<double> *> gVel,
                          QList<QList<double> *> gTorque);

    void saveLog( QList<double> TimeC, QList<QList<double> *> gPos, QList<QList<double> *> gPosErr,
                  QList<QList<double> *> gVel, QList<QList<double> *> gTorque,
                  QList < QList<double>* > jointPosRefmm );

    void finish();
};

#endif // ROBOTCONTROLLER_H
