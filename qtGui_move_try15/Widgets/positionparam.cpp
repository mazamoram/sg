#include "positionparam.h"
#include "ui_positionparam.h"

PositionParam::PositionParam(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PositionParam)
{
    ui->setupUi(this);
    this->ui->xLineEdit->setValidator( new QDoubleValidator() );
    this->ui->yLineEdit->setValidator( new QDoubleValidator() );
    this->ui->zLineEdit->setValidator( new QDoubleValidator() );
    this->ui->rLineEdit->setValidator( new QDoubleValidator() );
    this->ui->pLineEdit->setValidator( new QDoubleValidator() );
    this->ui->ywLineEdit->setValidator( new QDoubleValidator() );

}

PositionParam::~PositionParam()
{
    delete ui;
}

void PositionParam::refreshPose(){
    ui->xLineEdit->setText(QString::number(this->x));
    ui->yLineEdit->setText(QString::number(this->y));
    ui->zLineEdit->setText(QString::number(this->z));
    ui->rLineEdit->setText(QString::number(this->roll));
    ui->pLineEdit->setText(QString::number(this->pitch));
    ui->ywLineEdit->setText(QString::number(this->yaw));
}


void PositionParam::refreshPose(double x, double y, double z, double roll, double pitch, double yaw){

    this->x = x;
    this->y = y;
    this->z = z;
    this->roll = roll;
    this->pitch = pitch;
    this->yaw = yaw;
    refreshPose();
}

void PositionParam::on_buttonBox_accepted()
{
    this->x = this->ui->xLineEdit->text().toDouble();
    this->y = this->ui->yLineEdit->text().toDouble();
    this->z = this->ui->zLineEdit->text().toDouble();
    this->roll = this->ui->rLineEdit->text().toDouble();
    this->pitch = this->ui->pLineEdit->text().toDouble();
    this->yaw = this->ui->ywLineEdit->text().toDouble();
}
