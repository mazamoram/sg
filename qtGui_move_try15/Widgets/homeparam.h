#ifndef HOMEPARAM_H
#define HOMEPARAM_H

#include <QDialog>

namespace Ui {
class HomeParam;
}

class HomeParam : public QDialog
{
    Q_OBJECT

public:
    explicit HomeParam(QWidget *parent = 0);
    ~HomeParam();
    double x, y, z, roll, pitch, yaw;

private:
    Ui::HomeParam *ui;
};

#endif // HOMEPARAM_H
