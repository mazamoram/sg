#ifndef POSITIONPARAM_H
#define POSITIONPARAM_H

#include <QDialog>

namespace Ui {
class PositionParam;
}

class PositionParam : public QDialog
{
    Q_OBJECT

public:
    explicit PositionParam(QWidget *parent = 0);
    ~PositionParam();
    double x, y, z, roll, pitch, yaw;
    void refreshPose();
    void refreshPose(double x, double y, double z, double roll, double pitch, double yaw);
private slots:
    void on_buttonBox_accepted();

private:
    Ui::PositionParam *ui;
};

#endif // POSITIONPARAM_H
