#include "robotparameters.h"
#include "ui_robotparameters.h"

RobotParameters::RobotParameters(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RobotParameters)
{
    ui->setupUi(this);

    this->ui->diaBaseLineEdit->setValidator(new QDoubleValidator());
    this->ui->A1LineEdit->setValidator(new QDoubleValidator());
    this->ui->diaMobileLineEdit->setValidator(new QDoubleValidator());
    this->ui->A2LineEdit->setValidator(new QDoubleValidator());
    this->ui->L1LineEdit->setValidator(new QDoubleValidator());
    this->ui->L2LineEdit->setValidator(new QDoubleValidator());

}

RobotParameters::~RobotParameters()
{
    delete ui;
}

void RobotParameters::refreshParameters()
{
    this->ui->diaBaseLineEdit->setText(QString::number(this->diaBase));
    this->ui->A1LineEdit->setText(QString::number(this->A1));
    this->ui->diaMobileLineEdit->setText(QString::number(this->diaMobile));
    this->ui->A2LineEdit->setText(QString::number(this->A2));
    this->ui->L1LineEdit->setText(QString::number(this->L1));
    this->ui->L2LineEdit->setText(QString::number(this->L2));
}

void RobotParameters::on_buttonBox_accepted()
{
    this->diaBase = this->ui->diaBaseLineEdit->text().toDouble();
    this->A1 = this->ui->A1LineEdit->text().toDouble();
    this->diaMobile = this->ui->diaMobileLineEdit->text().toDouble();
    this->A2 = this->ui->A2LineEdit->text().toDouble();
    this->L1 = this->ui->L1LineEdit->text().toDouble();
    this->L2 = this->ui->L2LineEdit->text().toDouble();



    std::ofstream sgFileConfig;
    sgFileConfig.open("/home/mike/Documentos/2015/StewartGough/QtProjects/projects/qtGui_move_try15/ConfigFiles/SG.par");
    //sgFileConfig.open("ConfigFiles/SG.par");
    sgFileConfig    << this->ui->diaBaseLineEdit->text().toDouble() << ","
                    << this->ui->A1LineEdit->text().toDouble() << ","
                    << this->ui->diaMobileLineEdit->text().toDouble() << ","
                    << this->ui->A2LineEdit->text().toDouble() << ","
                    << this->ui->L1LineEdit->text().toDouble() << ","
                    << this->ui->L2LineEdit->text().toDouble() << "\n";


    sgFileConfig.close();
}
