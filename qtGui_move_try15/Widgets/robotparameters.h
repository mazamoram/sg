#ifndef ROBOTPARAMETERS_H
#define ROBOTPARAMETERS_H

#include <QDialog>
#include <QFile>
#include <iostream>
#include <fstream>

namespace Ui {
class RobotParameters;
}

class RobotParameters : public QDialog
{
    Q_OBJECT

public:
    explicit RobotParameters(QWidget *parent = 0);
    ~RobotParameters();
    void refreshParameters();

    double diaBase, A1, diaMobile, A2, L1, L2;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::RobotParameters *ui;
};

#endif // ROBOTPARAMETERS_H
