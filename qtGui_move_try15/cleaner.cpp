#include "cleaner.h"

cleaner::cleaner()
{

}

cleaner::~cleaner()
{

}

void cleaner::clearGCodeTrajectory(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                          QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center,
                          QList<double> *Radio, QList<double> *Velocity, QList<double> *t)
{
    Kind_traj->clear();
    Roll->clear();
    Pitch->clear();
    Yaw->clear();
    X->clear();
    Y->clear();
    Z->clear();
    X_center->clear();
    Y_center->clear();
    Z_center->clear();

    Radio->clear();
    Velocity->clear();
    t->clear();
}

void cleaner::clearJointTrajectory( QList< QList<double> > *pos_mm, QList< QList<double> > *vel_mmps,
                           QList< QList<double> > *acc_mmps2, QList< double > *time_s )
{
    pos_mm->clear();
    vel_mmps->clear();
    acc_mmps2->clear();
    time_s->clear();
}
