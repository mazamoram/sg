﻿#include <maincommands.h>

QElapsedTimer timer;

//logManager logF;


void dispGCodeInfo(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                   QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center,
                   QList<double> *Radio, QList<double> *Velocity, QList<double> *t){
    //qDebug() <<"Kind_traj: "<< *Kind_traj;
    qDebug() <<"Size kind: "<< Kind_traj->size();
    qDebug() <<"Size Roll: "<< Roll->size();
    qDebug() <<"Size Pitch: "<< Pitch->size();
    qDebug() <<"Size Yaw: "<< Yaw->size();
    qDebug() <<"Size X: "<< X->size();
    qDebug() <<"Size Y: "<< Y->size();
    qDebug() <<"Size Z: "<< Z->size();
    qDebug() <<"Size X_center: "<< X_center->size();
    qDebug() <<"Size Y_center: "<< Y_center->size();
    qDebug() <<"Size Z_center: "<< Z_center->size();

    qDebug() <<"Size Radio: "<< Radio->size();
    qDebug() <<"Size Velocity: "<< Velocity->size();
    qDebug() <<"Size t: "<< t->size();
}

void servoOff(Galil *g)
{
    g->command("MO");
}

void servoOn(Galil *g)
{
    g->command("SHABCDEF");
}

void numericDeriv(QList < QList<double>* > p, QList < QList<double>* > *v, double dt){

    for (int nAct = 0; nAct < p.size(); nAct++){
        for (int nPoints = 0; nPoints < p.at(0)->size()-1; nPoints ++)
        {
            v->at(nAct)->append((p.at(nAct)->at(nPoints+1) - p.at(nAct)->at(nPoints))/(dt));
        }
        v->at(nAct)->append( v->at(nAct)->last());
    }
}

void numericDeriv2(QList < QList<double>* > p, QList < QList<double>* > *a, double dt){

    for (int nAct = 0; nAct < p.size(); nAct++){
        for (int nPoints = 0; nPoints < p.at(0)->size()-2; nPoints ++)
        {
            a->at(nAct)->append((p.at(nAct)->at(nPoints+2) - 2*p.at(nAct)->at(nPoints+1) + p.at(nAct)->at(nPoints))/(dt*dt));
        }
        a->at(nAct)->append( a->at(nAct)->last());
        a->at(nAct)->append( a->at(nAct)->last());
    }
}



