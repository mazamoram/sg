#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>

#include "QFileDialog"
#include "QMessageBox"
#include <QDateTime>

#include "QList"
#include "QDebug"
#include "QString"
#include "iostream"
#include "fstream"

#include <QElapsedTimer>

#include "cleaner.h"
#include <Widgets/robotparameters.h>


class fileManager
{
public:
    fileManager();
    ~fileManager();

    QString logPath;
    std::ofstream refData;
    std::ofstream sensorData;
    cleaner clnr;
    QElapsedTimer timer;
    void saveRef(QList < QList<double>* > jointPosRefmm, QString fName = "ref.log");

    void saveSensor(QList<QList<double> *> gPos, QList<QList<double> *> gVel, QList<QList<double> *> gErr,
                                QList<QList<double> *> gTorque, QList<double> TimeC, QString fName= "sensor.log");

    void saveLogDialog(QList<QList<double> *> gPos, QList<QList<double> *> gVel, QList<QList<double> *> gPosErr,
                       QList<QList<double> *> gTorque, QList<double> TimeC, QList<QList<double> *> jointPosRefmm);

    bool loadGCode(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                    QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center, QList<double> *Radio,
                    QList<double> *Velocity, QList<double> *t );

    bool loadTrajectory(QList< QList<double> > *pos_mm, QList< QList<double> > *vel_mmps,
                         QList< QList<double> > *acc_mmps2, QList< double > *time_s);

    void readParameters(RobotParameters *rp);

};

#endif // FILEMANAGER_H
