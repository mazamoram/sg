#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>

// General includes
#include <QDebug>
#include <QString>
#include <QTextStream>
#include <QListIterator>
#include <QElapsedTimer>
#include <math.h>
#include <QMessageBox>
#include <QThread>


// Following includes are necessary for galil libraries
#include "Galil.h"   //vector string Galil
#include <iostream>  //cout
#include <sstream>   //ostringstream istringstream

// My includes
#include <conversions.h>
#include <sg.h>
#include <interpolator.h>
#include <trajgenerator.h>
#include <maincommands.h>
#include <filemanager.h>
#include <easyqwtcurve.h>
#include <Widgets/robotparameters.h>
#include <Widgets/positionparam.h>

#include <QLineEdit>


#include "gsl/gsl_poly.h"

//#include "qwt_plot.h"
//#include <qwt_plot_grid.h>
//#include <qwt_legend.h>
//#include <qwt_plot_curve.h>
//#include <qwt_symbol.h>
//#include <qwt_plot_magnifier.h>
//#include <qwt_plot_panner.h>
//#include <qwt_plot_picker.h>
//#include <qwt_picker_machine.h>

//#include <qwt_series_data.h>
//#include <qwt_plot_legenditem.h>

//typedef QList<QList<double> *> MyArray;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *ui;
    RobotParameters rp;
    PositionParam pp;
    fileManager fMan;
    trajGenerator trajGen;
    RobotController* rc;

    QThread* rControlThread;

    //----------------------------------------  

    QList<double> LecturaAux;
    QList<double> errorAux;

    //---------------------------------------------

    QList <QColor> colors;
    QList <easyQwtCurve* > jointPos, jointVel, jointAcc, jointJerk;
    QList <easyQwtCurve* > jointErrPos, jointMeasuredPos, jointMeasuredVel, jointMeasuredTorque;

    easyQwtCurve jointPos1, jointPos2, jointPos3, jointPos4, jointPos5, jointPos6;
    easyQwtCurve jointVel1, jointVel2, jointVel3, jointVel4, jointVel5, jointVel6;
    easyQwtCurve jointAcc1, jointAcc2, jointAcc3, jointAcc4, jointAcc5, jointAcc6;
    easyQwtCurve jointJerk1, jointJerk2, jointJerk3, jointJerk4, jointJerk5, jointJerk6;
    easyQwtCurve jointErrPos1, jointErrPos2, jointErrPos3, jointErrPos4, jointErrPos5, jointErrPos6;

    easyQwtCurve jointMeasuredPos1, jointMeasuredPos2, jointMeasuredPos3;
    easyQwtCurve jointMeasuredPos4, jointMeasuredPos5, jointMeasuredPos6;

    easyQwtCurve jointMeasuredVel1, jointMeasuredVel2, jointMeasuredVel3;
    easyQwtCurve jointMeasuredVel4, jointMeasuredVel5, jointMeasuredVel6;

    easyQwtCurve jointMeasuredTorque1, jointMeasuredTorque2, jointMeasuredTorque3;
    easyQwtCurve jointMeasuredTorque4, jointMeasuredTorque5, jointMeasuredTorque6;

    //---------------------------------------------

    QList< QList<double> > pos_mm, vel_mmps, acc_mmps2;
    QList< double > time_s;
    QList< QList<double> >  pos_pulses, pos_pulses_rel;

    //---------------------------------------------

    QList<int> Kind_traj;
    QList<double> Roll, Pitch, Yaw, X, Y, Z, X_center, Y_center, Z_center;

    QList<double> Radio, Velocity, t;

    QList < QList<double>* > jointPosRefmm;
    QList<double> jointPosRef1mm, jointPosRef2mm, jointPosRef3mm;
    QList<double> jointPosRef4mm, jointPosRef5mm, jointPosRef6mm;

    QList < QList<double>* > jointVelNumRefmm;
    QList<double> jointVelRef1mm, jointVelRef2mm, jointVelRef3mm;
    QList<double> jointVelRef4mm, jointVelRef5mm, jointVelRef6mm;

    QList < QList<double>* > jointAccNumRefmm;
    QList<double> jointAccRef1mm, jointAccRef2mm, jointAccRef3mm;
    QList<double> jointAccRef4mm, jointAccRef5mm, jointAccRef6mm;

    QList < QList<double>* > jointJerkNumRefmm;
    QList<double> jointJerkRef1mm, jointJerkRef2mm, jointJerkRef3mm;
    QList<double> jointJerkRef4mm, jointJerkRef5mm, jointJerkRef6mm;

    //---------------------------------------------

    QList< QLineEdit*>  encPulse, encMM;

    //---------------------------------------------

public slots:
    void execManualJoint(int axisId, QList<QList<double> > aux, double timeStep);

    void execManualWSpace(QString wsAxisId, double direction, double wSpaceStep, bool execute, double timeStep,
                          bool cyclic);

    void plotMeasurementsSlot(bool absMotion, QList<double> TimeC, QList<QList<double> *> gPos,
                              QList<QList<double> *> gPosErr, QList<QList<double> *> gVel,
                              QList<QList<double> *> gTorque);

    void saveLogSlot( QList<double> TimeC, QList<QList<double> *> gPos, QList<QList<double> *> gPosErr,
                                  QList<QList<double> *> gVel, QList<QList<double> *> gTorque,
                                  QList < QList<double>* > jointPosRefmm );

private slots:           
    void on_execGCodeButton_clicked();

    void on_execJointTrajButton_clicked();

    void on_aPlusPushButton_clicked();

    void on_fPlusPushButton_clicked();

    void on_servoOffButton_clicked();

    void on_servoOnButton_clicked();

    void on_aMinusPushButton_clicked();

    void on_fMinusPushButton_clicked();

    void on_bPlusPushButton_clicked();

    void on_bMinusPushButton_clicked();

    void on_cMinusPushButton_clicked();

    void on_cPlusPushButton_clicked();

    void on_dPlusPushButton_clicked();

    void on_dMinusPushButton_clicked();

    void on_ePlusPushButton_clicked();

    void on_eMinusPushButton_clicked();

    void on_startJoggingButton_clicked();

    void on_zPlusPushButton_clicked();

    void on_xPlusPushButton_clicked();

    void on_checkBox1_toggled(bool checked);

    void on_checkBox2_toggled(bool checked);

    void on_checkBox3_toggled(bool checked);

    void on_checkBox4_toggled(bool checked);

    void on_checkBox5_toggled(bool checked);

    void on_checkBox6_toggled(bool checked);

    void on_yPlusPushButton_clicked();

    void on_xMinusPushButton_clicked();

    void on_yMinusPushButton_clicked();

    void on_zMinusPushButton_clicked();

    void on_checkBox1_2_toggled(bool checked);

    void on_checkBox2_2_toggled(bool checked);

    void on_checkBox3_2_toggled(bool checked);

    void on_checkBox4_2_toggled(bool checked);

    void on_checkBox5_2_toggled(bool checked);

    void on_checkBox6_2_toggled(bool checked);

    void on_checkBox1_4_toggled(bool checked);

    void on_checkBox2_4_toggled(bool checked);

    void on_checkBox3_4_toggled(bool checked);

    void on_checkBox4_4_toggled(bool checked);

    void on_checkBox5_4_toggled(bool checked);

    void on_checkBox6_4_toggled(bool checked);

    void on_checkBox6_5_toggled(bool checked);

    void on_checkBox1_5_toggled(bool checked);

    void on_checkBox2_5_toggled(bool checked);

    void on_checkBox3_5_toggled(bool checked);

    void on_checkBox4_5_toggled(bool checked);

    void on_checkBox5_5_toggled(bool checked);

    void on_checkBox1_6_toggled(bool checked);

    void on_checkBox2_6_toggled(bool checked);

    void on_checkBox3_6_toggled(bool checked);

    void on_checkBox4_6_toggled(bool checked);

    void on_checkBox5_6_toggled(bool checked);

    void on_checkBox6_6_toggled(bool checked);

    void on_checkBox1_3_toggled(bool checked);

    void on_checkBox2_3_toggled(bool checked);

    void on_checkBox3_3_toggled(bool checked);

    void on_checkBox4_3_toggled(bool checked);

    void on_checkBox5_3_toggled(bool checked);

    void on_checkBox6_3_toggled(bool checked);

    void plotJoint(int axisId, QList< QList<double> > aux, double timeStep);

    void cleanMeasurementsGraph();

    void nAxisMove(int axisId, double direction);

    void nAxisMoveN(int axisId, int nTimes, double jointStep);

    void moveWSpace(QString wsAxisId, double direction);

    void jMove(int axisId);

    //private:

    void on_startJoggingButton_2_clicked();
    void moveWSpaceN(QString wsAxisId, int nTimes);

    void rotWSpace(QString rotAxisId, double direction);
    void rotWSpaceN(QString rotAxisId, int nTimes);

    void on_rollPlusPushButton_clicked();
    void on_pitchPlusPushButton_clicked();
    void on_rollMinusPushButton_clicked();
    void on_pitchMinusPushButton_clicked();

    //    void on_actionLog_Folder_triggered();
    //    void on_actionStart_logging_triggered();
    //    void on_actionStop_logging_triggered();
    void on_actionLog_Folder_triggered(bool checked);
    void on_actionSG_parameters_triggered();
    void on_actionSet_Position_triggered();


    void loadGCode();
    void loadJointTraj();
    void execGCode();
    void execJointTraj();
    void updateLastMove();
    void updateGuiPos();

    void on_actionLoad_G_Code_triggered();
    void on_actionLoad_Joint_Trajectory_triggered();
    void on_actionExecute_Joint_Trajectory_triggered();
    void on_actionExecute_G_Code_triggered();
    void on_actionInfo_triggered();
    void checkTimes();
    void on_timeIntervalJointLineEdit_editingFinished();
    void on_smplTimecomboBox_currentIndexChanged(int index);
    void on_checkBox1_8_toggled(bool checked);
    void on_checkBox2_8_toggled(bool checked);
    void on_checkBox3_8_toggled(bool checked);
    void on_checkBox4_8_toggled(bool checked);
    void on_checkBox5_8_toggled(bool checked);
    void on_checkBox6_8_toggled(bool checked);
    void on_checkBox1_7_toggled(bool checked);
    void on_checkBox2_7_toggled(bool checked);
    void on_checkBox3_7_toggled(bool checked);
    void on_checkBox4_7_toggled(bool checked);
    void on_checkBox5_7_toggled(bool checked);
    void on_checkBox6_7_toggled(bool checked);
    void on_actionQuintic_triggered();
    void on_actionDouble_S_triggered();

signals:
    void jMoveSignal(Galil *g,  int pot_dos, int axisId, QList < QList<double>* > jointPosRefmm );

};

#endif // MAINWINDOW_H
