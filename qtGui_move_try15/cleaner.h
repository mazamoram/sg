#ifndef CLEANER_H
#define CLEANER_H

#include "QList"

class cleaner
{
public:
    cleaner();
    ~cleaner();
    void clearGCodeTrajectory(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                              QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center,
                              QList<double> *Radio, QList<double> *Velocity, QList<double> *t);

    void clearJointTrajectory( QList< QList<double> > *pos_mm, QList< QList<double> > *vel_mmps,
                               QList< QList<double> > *acc_mmps2, QList< double > *time_s );

};

#endif // CLEANER_H
