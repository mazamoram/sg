#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T11:57:16
#
#-------------------------------------------------

QT       += core gui

#CONFIG += qwt release




greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = qtGui_move_try15
TEMPLATE = app



SOURCES += main.cpp\
           mainwindow.cpp \
           conversions.cpp \
           sg.cpp \
           interpolator.cpp \
           maincommands.cpp \
           easyqwtcurve.cpp \
    Widgets/robotparameters.cpp \
    Widgets/positionparam.cpp \
    filemanager.cpp \
    cleaner.cpp \
    trajgenerator.cpp \
    robotcontroller.cpp

HEADERS  += mainwindow.h \
            conversions.h \
            sg.h \
            interpolator.h \
            maincommands.h \
            easyqwtcurve.h \
    Widgets/robotparameters.h \
    Widgets/positionparam.h \
    filemanager.h \
    cleaner.h \
    trajgenerator.h \
    robotcontroller.h

FORMS    += mainwindow.ui \
    Widgets/robotparameters.ui \
    Widgets/positionparam.ui

# Galil Libraries
INCLUDEPATH += /usr/lib
LIBS += -L/usr/lib -lGalil
LIBS += -L/usr/lib -llapack -lblas -larmadillo
LIBS += -L/usr/lib  -lgsl -lgslcblas

#Qwt Libraries
INCLUDEPATH += /usr/local/qwt-6.1.2/include
LIBS += -L//usr/local/qwt-6.1.2/lib -lqwt


