#include "interpolator.h"
#include <math.h>
#include <qmath.h>
#include <QDebug>

interpolator::interpolator()
{

}

interpolator::~interpolator()
{

}



QList< QList<double> > interpolator::quinticInterp(double tf, double step, double qi, double qpi, double qppi,
                                                   double qf, double qpf, double qppf)
{

    double tf2 = tf*tf;
    double tf3 = tf2*tf;
    double tf4 = tf3*tf;
    double tf5 = tf4*tf;

    double a0 = qi;
    double a1 = qpi;
    double a2 = qppi/2.0;
    double a3 = (20.0*(qf-qi)-( 8.0*qpf+12.0*qpi)*tf - (3.0*qppi  -  qppf)*tf2)/(2*tf3);
    double a4 = (30.0*(qi-qf)+(14.0*qpf+16.0*qpi)*tf - (3.0*qppi-2.0*qppf)*tf2)/(2*tf4);
    double a5 = (12.0*(qf-qi)-( 6.0*qpf+ 6.0*qpi)*tf - (    qppi  -  qppf)*tf2)/(2*tf5);

    double auxt = 0;
    QList<double> pos;
    QList<double> vel;
    QList<double> acc;
    QList<double> jerk;
    while(auxt < tf)
    {
        auxt += step;
        pos << a0 + a1*auxt +   a2*pow(auxt,2) +   a3*pow(auxt,3) +    a4*pow(auxt,4) +    a5*pow(auxt,5);
        vel <<      a1      + 2*a2*auxt        + 3*a3*pow(auxt,2) +  4*a4*pow(auxt,3) +  5*a5*pow(auxt,4);
        acc <<                2*a2             + 6*a3*auxt        + 12*a4*pow(auxt,2) + 20*a5*pow(auxt,3);
        jerk <<                                + 6*a3             + 24*a4*auxt        + 60*a5*pow(auxt,2);
    }

    QList< QList<double> > aux;
    aux << pos << vel << acc << jerk;

    return aux;
}

QList< QList<double> > interpolator::sInterp(double step, double vmax, double J, double pi, double vi, double ai,
                                             double pf, double vf, double af)
{
    qDebug() <<"params: "<<step<<","<<vmax<<","<<J<<","<<pi<<","<<vi<<","<<ai<<","<<pf<<","<<vf<<","<<af;

    double sign = 1.0;
    if ((pf-pi)<0){ sign = -1.0; }
    qDebug()<<"sign: "<<sign;
    vmax = sign*vmax;
    J = sign*J;

    qDebug() <<"vmax, J: " << vmax, J;
    qDebug() <<(pow(ai,2) - 2*J*vi + 2*vmax*J)/2;

    double ac1 = sign*0.90*sqrt((pow(ai,2) - 2*J*vi + 2*vmax*J)/2);
    double ac2 = sign*0.90*sqrt((pow(af,2) - 2*J*vf + 2*vmax*J)/2);

    qDebug()<<"ac1, ac2: "<<ac1 <<"," <<ac2;

    double t1 = (ac1-ai)/J;
    double t3 = (ac1)/J;
    double t5 = (ac2)/J;
    double t7 =  (af+ac2)/J;

    //%------------------------------------------

    double t2 = -(2*ac1*ac1 - ai*ai + 2*J*vi - 2*J*vmax)/(2*J*ac1);
    double t6 = -(2*ac2*ac2 - af*af + 2*J*vf - 2*J*vmax)/(2*J*ac2);

    qDebug()<<"t2, t6: "<<t2 <<", "<<t6;

    double Pac1 = 0.0;
    double Pac2 = 0.0;
    double Pac0 = 0.0;
    double t4 = 0.0;
    double tt = 0.0;


    double auxt = 0.0;
    QList<double> pos;
    QList<double> vel;
    QList<double> acc;
    QList<double> jerk;
    QList< QList<double> > aux;


    if (t2 > 0 && t6 >0) {
        Pac1 =  vi*t1 +ai/2*pow(t1,2) + J/6*pow(t1,3)
                +(vi +ai*t1 + J/2*pow(t1,2))*(t2)
                + ac1/2*pow(t2,2)
                +(vi +ai*t1 + J/2*pow(t1,2) + ac1*(t2))*(t3)
                + ac1/2*pow(t3,2)-J/6*pow(t3,3);

        Pac2 =  vmax*t5 - J/6*pow(t5,3)
                +(vmax - J/2*pow(t5,2))*(t6)
                - ac2/2*pow(t6,2)
                +(vmax - J/2*pow(t5,2) - ac2*(t6))*(t7)
                - ac2/2*pow(t7,2) + J/6*pow(t7,3);

        qDebug() <<"Pac1, pac2: "<<Pac1<<", "<<Pac2;

        if (qFabs(Pac1) + qFabs(Pac2) <= qFabs(pf-pi)){

            Pac0 = sign*(pf-pi) - sign*Pac1 - sign*Pac2;
            t4 = sign*Pac0/vmax;

            tt = t1 + t2 + t3 + t4 + t5 +t6 + t7;

            while(auxt < tt)
            {
                auxt += step;


                if (auxt <= t1){
                    pos <<  pi + vi*auxt + ai/2*pow(auxt,2) + J/6*pow(auxt, 3);
                    vel <<  vi + ai*auxt + J/2*pow(auxt,2);
                    acc <<  ai + J*auxt;
                    jerk << J;

                    //Jk(i) = J;
                    //A(i) = ai + J*t(i);
                    //V(i) = vi +ai*t(i) + J/2*t(i)^2;
                    //P(i) = pi + vi*t(i) +ai/2*t(i)^2 + J/6*t(i)^3;
                }else if (auxt<= t1 + t2) {
                    pos <<  pi + vi*t1 +ai/2*pow(t1, 2) + J/6*pow(t1,3) + (vi +ai*t1 + J/2*pow(t1,2))*(auxt-t1) + ac1/2*pow(auxt-t1, 2);
                    vel <<  vi + ai*t1 + J/2*pow(t1,2) + ac1*(auxt-t1);
                    acc <<  ac1;
                    jerk << 0.0;

                    //Jk(i) = 0;
                    //A(i) = ac1;
                    //V(i) = vi +ai*t1 + J/2*t1^2 + ac1*(t(i)-t1);
                    //P(i) = pi + vi*t1 +ai/2*t1^2 + J/6*t1^3 ...
                    //    +(vi +ai*t1 + J/2*t1^2)*(t(i)-t1) ...
                    //    +ac1/2*(t(i)-t1)^2;

                }  else if (auxt<= t1 + t2 + t3){
                    pos << pi + vi*t1 +ai/2*pow(t1,2) + J/6*pow(t1,3) + (vi +ai*t1 + J/2*pow(t1,2))*(t2) + ac1/2*pow(t2,2) +
                          (vi + ai*t1 + J/2*pow(t1,2) + ac1*(t2))*(auxt-t1-t2) + ac1/2*pow(auxt-t1-t2,2)-J/6*pow(auxt-t1-t2,3);
                    vel << vi +ai*t1 + J/2*pow(t1,2) + ac1*(t2) + ac1*(auxt-t1-t2) - J/2*pow(auxt-t1-t2,2);
                    acc << ac1 - J*(auxt -t1 -t2);
                    jerk << -J;

                    //Jk(i) = -J;
                    //A(i) = ac1 -J*(t(i) -t1 -t2);
                    //V(i) = vi +ai*t1 + J/2*t1^2 + ac1*(t2)...
                    //    +ac1*(t(i)-t1-t2)-J/2*(t(i)-t1-t2)^2;
                    //P(i) = pi + vi*t1 +ai/2*t1^2 + J/6*t1^3 ...
                    //    +(vi +ai*t1 + J/2*t1^2)*(t2) ...
                    //    + ac1/2*(t2)^2 ...
                    //    +(vi +ai*t1 + J/2*t1^2 + ac1*(t2))*(t(i)-t1-t2)...
                    //    + ac1/2*(t(i)-t1-t2)^2-J/6*(t(i)-t1-t2)^3;

                }else if (auxt<= t1 + t2 + t3 + t4){
                    pos << pi + vi*t1 +ai/2*pow(t1,2) + J/6*pow(t1,3) + (vi +ai*t1 + J/2*pow(t1,2))*(t2) + ac1/2*pow(t2,2) +
                          (vi + ai*t1 + J/2*pow(t1,2) + ac1*(t2))*(t3) + ac1/2*pow(t3,2)-J/6*pow(t3,3) + vmax*(auxt-t1-t2-t3);
                    vel << vmax;
                    acc << 0.0;
                    jerk << 0.0;

                    //Jk(i) = 0;
                    //A(i) = 0;
                    //V(i) = vmax;
                    //P(i) = pi + vi*t1 +ai/2*t1^2 + J/6*t1^3 ...
                    //    +(vi +ai*t1 + J/2*t1^2)*(t2) ...
                    //   + ac1/2*(t2)^2 ...
                    //    +(vi +ai*t1 + J/2*t1^2 + ac1*(t2))*(t3)...
                    //    + ac1/2*(t3)^2-J/6*(t3)^3 ...
                    //    + vmax*(t(i)-t1-t2-t3);
                }
                else if (auxt<= t1 + t2 + t3 + t4 +t5){
                    pos << pi + vi*t1 +ai/2*pow(t1,2) + J/6*pow(t1,3) + (vi +ai*t1 + J/2*pow(t1,2))*(t2) + ac1/2*pow(t2,2) +
                          (vi + ai*t1 + J/2*pow(t1,2) + ac1*(t2))*(t3) + ac1/2*pow(t3,2)-J/6*pow(t3,3) + vmax*(t4) +
                           vmax*(auxt -t1 -t2 -t3 -t4) - J/6*pow(auxt -t1 -t2 -t3 -t4,3);
                    vel << vmax -J/2*pow(auxt -t1 -t2 -t3 -t4,2);
                    acc << -J*(auxt -t1 -t2 -t3 -t4);
                    jerk << -J;

                    //Jk(i) = -J;
                    //A(i) = -J*(t(i) -t1 -t2 -t3 -t4);
                    //V(i) = vmax-J/2*(t(i) -t1 -t2 -t3 -t4)^2;
                    //P(i) = pi + vi*t1 +ai/2*t1^2 + J/6*t1^3 ...
                    //    +(vi +ai*t1 + J/2*t1^2)*(t2) ...
                    //    + ac1/2*(t2)^2 ...
                    //    +(vi +ai*t1 + J/2*t1^2 + ac1*(t2))*(t3)...
                    //   + ac1/2*(t3)^2-J/6*(t3)^3 ...
                    //    + vmax*(t4) ...
                    //    + vmax*(t(i) -t1 -t2 -t3 -t4) - J/6*(t(i) -t1 -t2 -t3 -t4)^3;
                }else if (auxt<= t1 + t2 + t3 + t4 +t5 + t6){
                    pos << pi + vi*t1 +ai/2*pow(t1,2) + J/6*pow(t1,3) + (vi +ai*t1 + J/2*pow(t1,2))*(t2) + ac1/2*pow(t2,2) +
                          (vi + ai*t1 + J/2*pow(t1,2) + ac1*(t2))*(t3) + ac1/2*pow(t3,2)-J/6*pow(t3,3) + vmax*(t4) +
                           vmax*(t5) - J/6*pow(t5,3) + (vmax-J/2*pow(t5,2))*(auxt -t1 -t2 -t3 -t4-t5) - ac2/2*pow(auxt -t1 -t2 -t3 -t4-t5,2);
                    vel << vmax -J/2*pow(t5,2) - ac2*(auxt -t1 -t2 -t3 -t4-t5);
                    acc << -ac2;
                    jerk << 0.0;

                    //Jk(i) = 0;
                    //A(i) = -ac2;
                    //V(i) = vmax-J/2*(t5)^2 -ac2*(t(i) -t1 -t2 -t3 -t4-t5);
                    //P(i) = pi + vi*t1 +ai/2*t1^2 + J/6*t1^3 ...
                    //    +(vi +ai*t1 + J/2*t1^2)*(t2) ...
                    //    + ac1/2*(t2)^2 ...
                    //    +(vi +ai*t1 + J/2*t1^2 + ac1*(t2))*(t3)...
                    //    + ac1/2*(t3)^2-J/6*(t3)^3 ...
                    //    + vmax*(t4) ...
                    //    + vmax*(t5) - J/6*(t5)^3 ...
                    //    + (vmax-J/2*(t5)^2)*(t(i) -t1 -t2 -t3 -t4-t5)...
                    //    -ac2/2*(t(i) -t1 -t2 -t3 -t4-t5)^2;

                }else if (auxt<= t1 + t2 + t3 + t4 +t5 + t6 + t7){
                    pos << pi + vi*t1 +ai/2*pow(t1,2) + J/6*pow(t1,3) + (vi +ai*t1 + J/2*pow(t1,2))*(t2) + ac1/2*pow(t2,2) +
                          (vi + ai*t1 + J/2*pow(t1,2) + ac1*(t2))*(t3) + ac1/2*pow(t3,2)-J/6*pow(t3,3) + vmax*(t4) +
                           vmax*(t5) - J/6*pow(t5,3) + (vmax-J/2*pow(t5,2))*(t6) - ac2/2*pow(t6,2) +
                          (vmax-J/2*pow(t5,2) -ac2*(t6)) *(auxt-t1-t2-t3-t4-t5-t6)  - ac2/2*pow(auxt-t1-t2-t3-t4-t5-t6,2) +
                           J/6*pow(auxt-t1-t2-t3-t4-t5-t6,3);

                    vel << vmax -J/2*pow(t5,2) - ac2*(t6) - ac2*(auxt-t1-t2-t3-t4-t5-t6) +J/2*pow(auxt-t1-t2-t3-t4-t5-t6,2);
                    acc << -ac2 + J*(auxt -t1 -t2 -t3 -t4 -t5 -t6);
                    jerk << J;
                    //Jk(i) = J;
                    //A(i) = -ac2 + J*(t(i) -t1 -t2 -t3 -t4 -t5 -t6);
                    //V(i) = vmax-J/2*(t5)^2 -ac2*(t6) - ac2*(t(i)-t1-t2-t3-t4-t5-t6)...
                    //    +J/2*(t(i)-t1-t2-t3-t4-t5-t6)^2;
                    //P(i) = pi + vi*t1 +ai/2*t1^2 + J/6*t1^3 ...
                    //    +(vi +ai*t1 + J/2*t1^2)*(t2) ...
                    //    + ac1/2*(t2)^2 ...
                    //    +(vi +ai*t1 + J/2*t1^2 + ac1*(t2))*(t3)...
                    //    + ac1/2*(t3)^2-J/6*(t3)^3 ...
                    //    + vmax*(t4) ...
                    //    + vmax*(t5) - J/6*(t5)^3 ...
                    //    + (vmax-J/2*(t5)^2)*(t6)...
                    //    -ac2/2*(t6)^2 ...
                    //   + (vmax-J/2*(t5)^2 -ac2*(t6)) *(t(i)-t1-t2-t3-t4-t5-t6) ...
                    //    - ac2/2*(t(i)-t1-t2-t3-t4-t5-t6)^2 ...
                    //   +J/6*(t(i)-t1-t2-t3-t4-t5-t6)^3;
                }



            }

        }
    }

    aux << pos << vel << acc << jerk;

    //qDebug()<<aux;
    return aux;
}




QList< QList<double> > interpolator::interpolate(int interpolatorType, double tf, double step, double qi, double qpi, double qppi,
                                                 double qf, double qpf, double qppf, double vmax, double jerkmax)
{
    if (interpolatorType == 1) // Lineal interpolator
    {
        return this->quinticInterp(tf, step, qi, qpi, qppi, qf, qpf, qppf);
    }
    else{
        return this->sInterp(step, vmax, jerkmax,qi, qpi, qppi, qf, qpf, qppf);
    }
}
