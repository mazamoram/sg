#ifndef TRAJGENERATOR_H
#define TRAJGENERATOR_H

#include <QObject>
#include <QDebug>
#include <QList>
#include <QMessageBox>
#include <qvector3d.h>
#include <qmatrix4x4.h>
#include <QThread>

#include <interpolator.h>
#include <sg.h>
#include <robotcontroller.h>



class trajGenerator: public QObject
{
    Q_OBJECT

public:
    trajGenerator();
    ~trajGenerator();

    bool genGCodeTraj(QList<int> *Kind_traj, QList<double> *Roll, QList<double> *Pitch, QList<double> *Yaw, QList<double> *X,
                      QList<double> *Y, QList<double> *Z, QList<double> *X_center, QList<double> *Y_center, QList<double> *Z_center, QList<double> *Radio,
                      QList<double> *Velocity, QList<double> *t , double timeStep, sg mysgp, QList<QList<double> *> jointPosRefmm);

    void nAxisMove(int axisId, double direction, double jointStep, int pot_dos,
                   int interpolationType, double timeInterval, double maxJerk,
                   QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                   QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm);

    void nAxisMoveN(int axisId, int nTimes, double jointStep, int pot_dos,
                    int interpolationType, double timeInterval, double maxJerk,
                    QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                    QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm);

    void moveWSpace(QString wsAxisId, double direction, double wSpaceStep, int pot_dos,
                    int interpolationType, double timeInterval, double maxJerk,
                    QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                    QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm, sg mysgp );

    void moveWSpaceN(QString wsAxisId, int nTimes, double wSpaceStep, int pot_dos,
                     int interpolationType, double timeInterval, double maxJerk,
                     QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                     QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm, sg mysgp );

    void rotWSpace(QString rotAxisId, double direction, double angleStep, int pot_dos,
                   int interpolationType, double timeInterval, double maxJerk,
                   QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                   QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm, sg mysgp );

    void rotWSpaceN(QString rotAxisId, int nTimes, double angleStep, int pot_dos,
                    int interpolationType, double timeInterval, double maxJerk,
                    QList < QList<double>* > jointPosRefmm, QList<QList<double> *> jointVelNumRefmm,
                    QList<QList<double> *> jointAccNumRefmm, QList<QList<double> *> jointJerkNumRefmm, sg mysgp );

    double offset;

signals:
    void execManualJointSignal(int axisId, QList< QList<double> > aux, double timeStep);

    void execManualWSpaceSignal(QString wsAxisId, double direction, double wSpaceStep, bool execute, double timeStep,
                                bool cyclic);
};



#endif // TRAJGENERATOR_H
