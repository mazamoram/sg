#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

//#include <string>
//#include <qfile.h>

// Variables declaration
//---------------------------------------------
//double offset = 1246.10791015625;
double offset = 1158.00085449218700;
int pot_dos = 4; // n value for DT command
int interv = pow(2,pot_dos);
//Galil* g;
Galil g;
sg mysgp;
double tmpTotalTime = 0.0;
double tmpElapsedTime = 0.0;
double tmpPercentage = 0.0;
bool trajecLoaded = false;
bool gCodeLoaded = false;
bool executeTraj = false;
bool executeGcode = false;
bool saveLog = false;
//---------------------------------------------

int interpolationType = 1;
//---------------------------------------------

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->maxJerkLabel->setVisible(false);
    ui->maxJerkLineEdit->setVisible(false);
    ui->maxJerkUnitLabel->setVisible(false);
    //---------------------------------------------

    fMan.readParameters(&rp);
    rp.refreshParameters();
    mysgp.setConfiguration( rp.diaBase/2, rp.A1/2, rp.diaMobile/2, rp.A2/2, rp.L1, rp.L2);
    mysgp.setPose(0,0,1278,0,0,0);

    trajGen.offset = offset;
    //---------------------------------------------
    ui->xPosLineEdit->setText(QString::number(mysgp.x));
    ui->yPosLineEdit->setText(QString::number(mysgp.y));
    ui->zPosLineEdit->setText(QString::number(mysgp.z));
    ui->rollPosLineEdit->setText(QString::number(mysgp.roll));
    ui->pitchPosLineEdit->setText(QString::number(mysgp.pitch));
    ui->yawPosLineEdit->setText(QString::number(mysgp.yaw));
    //---------------------------------------------
    this->statusBar()->setSizeGripEnabled(false);
    //this->ui->loadTrajectoryButton->setToolTip("End-Effector Trajectory  mm");

    this->ui->stepSizeJointLineEdit->setValidator(new QDoubleValidator());
    this->ui->timeIntervalJointLineEdit->setValidator(new QDoubleValidator());
    this->ui->nJogComboBox->setValidator(new QIntValidator());
    //---------------------------------------------
    this->ui->stepSizeJointLineEdit->setValidator(new QDoubleValidator());
    this->ui->stepSizeGradJointLineEdit->setValidator(new QDoubleValidator());
    this->ui->timeIntervalJointLineEdit->setValidator(new QDoubleValidator());
    this->ui->maxJerkLineEdit->setValidator(new QDoubleValidator());
    //---------------------------------------------
    this->ui->qwtPlot->setAxisTitle(QwtPlot::xBottom,"Time (ms)");
    this->ui->qwtPlot->setAxisTitle(QwtPlot::yLeft,"Joint Position (mm)");
    this->ui->qwtPlot1->setAxisTitle(QwtPlot::xBottom,"Time (ms)");
    this->ui->qwtPlot1->setAxisTitle(QwtPlot::yLeft,"Joint Velocity (mm/s)");
    this->ui->qwtPlot2->setAxisTitle(QwtPlot::xBottom,"Time (ms)");
    this->ui->qwtPlot2->setAxisTitle(QwtPlot::yLeft,"Joint Accel (mm/s2)");
    this->ui->qwtPlot3->setAxisTitle(QwtPlot::xBottom,"Time (ms)");
    this->ui->qwtPlot3->setAxisTitle(QwtPlot::yLeft,"Joint Jerk (mm/s3)");
    this->ui->qwtPlot4->setAxisTitle(QwtPlot::xBottom,"Time (ms)");
    this->ui->qwtPlot4->setAxisTitle(QwtPlot::yLeft,"Joint Position Error (mm)");


    //---------------------------------------------

    colors << QColor(Qt::red) << QColor(Qt::green) << QColor(Qt::blue)
           << QColor(Qt::magenta) << QColor(Qt::black) << QColor(Qt::cyan);

    jointPos << &jointPos1 << &jointPos2 << &jointPos3 << &jointPos4 << &jointPos5 << &jointPos6;
    jointVel << &jointVel1 << &jointVel2 << &jointVel3 << &jointVel4 << &jointVel5 << &jointVel6;
    jointAcc << &jointAcc1 << &jointAcc2 << &jointAcc3 << &jointAcc4 << &jointAcc5 << &jointAcc6;
    jointJerk << &jointJerk1 << &jointJerk2 << &jointJerk3 << &jointJerk4 << &jointJerk5 << &jointJerk6;
    jointErrPos << &jointErrPos1 << &jointErrPos2 << &jointErrPos3 << &jointErrPos4 << &jointErrPos5 << &jointErrPos6;

    jointMeasuredPos << &jointMeasuredPos1 << &jointMeasuredPos2 << &jointMeasuredPos3 << &jointMeasuredPos4
                     << &jointMeasuredPos5 << &jointMeasuredPos6;
    jointMeasuredVel << &jointMeasuredVel1 << &jointMeasuredVel2 << &jointMeasuredVel3 << &jointMeasuredVel4
                     << &jointMeasuredVel5 << &jointMeasuredVel6;
    jointMeasuredTorque << &jointMeasuredTorque1 << &jointMeasuredTorque2 << &jointMeasuredTorque3 << &jointMeasuredTorque4
                        << &jointMeasuredTorque5 << &jointMeasuredTorque6;


    encPulse << ui->mPulLineEdit_1 << ui->mPulLineEdit_2 << ui->mPulLineEdit_3 << ui->mPulLineEdit_4 << ui->mPulLineEdit_5
             << ui->mPulLineEdit_6;

    encMM << ui->mMMLineEdit_1 << ui->mMMLineEdit_2 << ui->mMMLineEdit_3 << ui->mMMLineEdit_4 << ui->mMMLineEdit_5
          << ui->mMMLineEdit_6;


    for(int i=0; i < jointPos.size(); i++)
    {
        jointPos.at(i)->set(ui->qwtPlot, colors.at(i), 1, Qt::DashLine);
        jointVel.at(i)->set(ui->qwtPlot1, colors.at(i), 1, Qt::DashLine);
        jointAcc.at(i)->set(ui->qwtPlot2, colors.at(i), 1, Qt::DashLine);
        jointJerk.at(i)->set(ui->qwtPlot3, colors.at(i), 1, Qt::DashLine);
        jointErrPos.at(i)->set(ui->qwtPlot4, colors.at(i));
        jointMeasuredPos.at(i)->set(ui->qwtPlot, colors.at(i));
        jointMeasuredVel.at(i)->set(ui->qwtPlot1, colors.at(i));
        jointMeasuredTorque.at(i)->set(ui->qwtPlot5, colors.at(i));
    }

    jointPosRefmm << &jointPosRef1mm << &jointPosRef2mm << &jointPosRef3mm << &jointPosRef4mm << &jointPosRef5mm
                  << &jointPosRef6mm;

    jointVelNumRefmm << &jointVelRef1mm << &jointVelRef2mm << &jointVelRef3mm << &jointVelRef4mm << &jointVelRef5mm
                     << &jointVelRef6mm;

    jointAccNumRefmm << &jointAccRef1mm << &jointAccRef2mm << &jointAccRef3mm << &jointAccRef4mm << &jointAccRef5mm
                     << &jointAccRef6mm;

    jointJerkNumRefmm << &jointJerkRef1mm << &jointJerkRef2mm << &jointJerkRef3mm << &jointJerkRef4mm << &jointJerkRef5mm
                      << &jointJerkRef6mm;


    //--------------------------
    rc = new RobotController();

    //qRegisterMetaType< Galil* >("Galil*");
    qRegisterMetaType< QList<QList<double>*> >("QList<QList<double>*>");

    QObject::connect(&trajGen, &trajGenerator::execManualJointSignal, this, &MainWindow::execManualJoint,
                     Qt::QueuedConnection);

    QObject::connect(&trajGen, &trajGenerator::execManualWSpaceSignal, this, &MainWindow::execManualWSpace,
                     Qt::QueuedConnection);


    QObject::connect(rc, SIGNAL(updatePlotSignal(bool,QList<double>,QList<QList<double>*>,QList<QList<double>*>,
                                                 QList<QList<double>*>,QList<QList<double>*>)),
                     this, SLOT(plotMeasurementsSlot(bool,QList<double>,QList<QList<double>*>,QList<QList<double>*>,
                                                     QList<QList<double>*>,QList<QList<double>*>)),
                     Qt::QueuedConnection);

    QObject::connect(rc, SIGNAL(saveLog(QList<double>,QList<QList<double>*>,QList<QList<double>*>,QList<QList<double>*>,
                                        QList<QList<double>*>,QList<QList<double>*>)),
                     this, SLOT(saveLogSlot(QList<double>,QList<QList<double>*>,QList<QList<double>*>,QList<QList<double>*>,
                                            QList<QList<double>*>,QList<QList<double>*>)),
                     Qt::QueuedConnection);

    QObject::connect(this, SIGNAL(jMoveSignal(Galil*,int,int,QList<QList<double>*>)),
                     rc, SLOT(jMoveSlot(Galil*,int,int,QList<QList<double>*>)) );




    //--------------------------
    rControlThread = new QThread;
    rc->moveToThread(rControlThread);
    ////connect(rc, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
    //connect(rControlThread, SIGNAL(started()), rc, SLOT(process()));
    QObject::connect(rc, SIGNAL(finished()), rControlThread, SLOT(quit()));
    ////connect(rc, SIGNAL(finished()), rc, SLOT(deleteLater()));
    ////connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));



    //--------------------------

    int i;
    /* coefficients of P(x) =  -1 + x^5  */
    double a[6] = { -1, 0, 0, 0, 0, 1 };
    double z[10];

    gsl_poly_complex_workspace * w
            = gsl_poly_complex_workspace_alloc (6);

    gsl_poly_complex_solve (a, 6, w, z);

    gsl_poly_complex_workspace_free (w);


    for (i = 0; i < 5; i++)
    {
        qDebug() << z[2*i] <<" "<< z[2*i +1];
        //        printf ("z%d = %+.18f %+.18f\n",
        //                i, z[2*i], z[2*i+1]);
    }
    //-------------------

    qDebug()<<"From the ui thread: "<<QThread::currentThreadId();

}

MainWindow::~MainWindow()
{
    delete ui;
}


void  MainWindow::plotJoint(int axisId, QList< QList<double> > aux, double timeStep){
    for(int i=0; i<6; i++){
        if(i!=(axisId-1)){
            jointPos.at(i)->hline(0,0, aux.at(0).size()*timeStep);
            jointVel.at(i)->hline(0,0, aux.at(1).size()*timeStep);
            jointAcc.at(i)->hline(0,0, aux.at(2).size()*timeStep);
            jointJerk.at(i)->hline(0,0, aux.at(3).size()*timeStep);
        }else{

            jointPos.at(i)->plot(timeStep, aux.at(0),0, -rc->gPos.at(i)->last()*pulses2mmFactor);
            //jointPos.at(i)->plot(timeStep, aux.at(0));
            jointVel.at(i)->plot(timeStep, aux.at(1));
            jointAcc.at(i)->plot(timeStep, aux.at(2));
            jointJerk.at(i)->plot(timeStep, aux.at(3));
        }
    }
}

void MainWindow::cleanMeasurementsGraph(){
    for(int i=0; i < jointMeasuredPos.size(); i++){
        jointMeasuredPos.at(i)->clear();
        jointErrPos.at(i)->clear();
        jointMeasuredVel.at(i)->clear();
        jointMeasuredTorque.at(i)->clear();
    }


}

void MainWindow::plotMeasurementsSlot(bool absMotion, QList<double> TimeC, QList<QList<double> *> gPos,
                                      QList<QList<double> *> gPosErr, QList<QList<double> *> gVel,
                                      QList<QList<double> *> gTorque){
    for(int i=0; i < jointMeasuredPos.size(); i++){
        if(absMotion){
            jointMeasuredPos.at(i)->updatePlot(TimeC, *gPos.at(i), TimeC.at(0), 0, 1, pulses2mmFactor );
        }
        else{
            jointMeasuredPos.at(i)->updatePlot(TimeC, *gPos.at(i), TimeC.at(0), gPos.at(i)->at(0), 1, pulses2mmFactor );
        }
        jointErrPos.at(i)->updatePlot(TimeC, *gPosErr.at(i), TimeC.at(0), 0, 1, pulses2mmFactor );
        jointMeasuredVel.at(i)->updatePlot(TimeC, *gVel.at(i), TimeC.at(0), 0, 1, pulses2mmFactor );
        jointMeasuredTorque.at(i)->updatePlot(TimeC, *gTorque.at(i), TimeC.at(0), 0, 1, 1 );
    }
}

void MainWindow::saveLogSlot( QList<double> TimeC, QList<QList<double> *> gPos, QList<QList<double> *> gPosErr,
                              QList<QList<double> *> gVel, QList<QList<double> *> gTorque,
                              QList < QList<double>* > jointPosRefmm ){
    if (saveLog){
        fMan.saveLogDialog(gPos, gVel, gPosErr, gTorque, TimeC, jointPosRefmm);
    }
}

void MainWindow::updateLastMove(){
    double incStep;
    //for(int i=0; i<encPulse.size(); i++){
    //    incStep = gPos.at(i)->last() -gPos.at(i)->at(0);;
    //    encPulse.at(i)->setText( QString::number(incStep) );
    //    encMM.at(i)->setText( QString::number(incStep*pulses2mmFactor) );
    //}
}


void MainWindow::jMove(int axisId)
{
    //clearMeasurements();

    //    QString pot_dos_str = QString::number(pot_dos);
    //    QString DTN = QString("DT ")+ pot_dos_str;
    //    g.command("SHABCDEF");
    //    //g.command("CMF");
    //    g.command("CMABCDEF");
    //    qDebug()<<"Se limpio? "<<atoi( g.command("CM?").c_str() );
    //    g.command("CMABCDEF");
    //    g.command(DTN.toUtf8().constData());
    //    qDebug()<< DTN.toUtf8().constData();

    //    QString CD("");

    //    int axisIdAux = axisId;
    //    if(axisId==0){
    //        axisIdAux = 1;
    //    }

    //    int Bloque = 100;

    //    if (Bloque > jointPosRefmm.at(axisIdAux-1)->size())
    //    {
    //        Bloque = jointPosRefmm.at(axisIdAux-1)->size();

    //        for (int i = 1; i<Bloque;i++)
    //        {
    //            CD = createCD(axisId, jointPosRefmm, i);
    //            qDebug()<< CD;
    //            g.command(CD.toUtf8().constData());
    //            qDebug()<<"p0: "<<CD.toUtf8().constData();
    //        }
    //        g.command("CD 0,0,0,0,0,0=0"); // Indica el punto final del contorno
    //    }
    //    else
    //    {
    //        for (int i = 1; i<Bloque;i++)
    //        {
    //            CD = createCD(axisId, jointPosRefmm, i);
    //            qDebug()<< CD;
    //            g.command(CD.toUtf8().constData());
    //            qDebug()<<"p0: "<<CD.toUtf8().constData();
    //        }

    //        // Luego de que termine el contorno
    //        // el comando CM permite limpiar el buffer siempre y cuando los motores se
    //        // encuentren encendidos
    //        int contador = 1;
    //        int Buffer = 510;

    //        while(Buffer != 511)
    //        {
    //            Buffer = atoi( g.command("CM?").c_str() ) ;
    //            qDebug()<<"-----------"<<Buffer;
    //            if ( Buffer > 150 && Buffer < 500 && contador ==1)
    //            {
    //                if (jointPosRefmm.at(axisIdAux-1)->size() - Bloque< 100)
    //                {
    //                    for (int ix = Bloque; ix < jointPosRefmm.at(axisIdAux-1)->size(); ix++)
    //                    {
    //                        CD = createCD(axisId, jointPosRefmm, ix);
    //                        g.command(CD.toUtf8().constData());
    //                        qDebug() <<"p1: "<< CD.toUtf8().constData();
    //                    }
    //                    contador = 2;
    //                    g.command("CD 0,0,0,0,0,0=0"); //indica el punto final del contorno
    //                }
    //                else
    //                {
    //                    for (int ix = Bloque; ix < Bloque + 100; ix++)
    //                    {
    //                        CD = createCD(axisId, jointPosRefmm, ix);
    //                        g.command(CD.toUtf8().constData());
    //                        qDebug()<<"p2: "<<CD.toUtf8().constData();
    //                    }
    //                    Bloque = Bloque + 100;
    //                }
    //            }

    //            TimeC << atof( g.command("MG TIME").c_str() );
    //            for(int jdx = 0; jdx < gPos.size(); jdx++){
    //                gPos.at(jdx)->append( atof( g.command( TP.at(jdx).toUtf8().constData() ).c_str() ));
    //                gPosErr.at(jdx)->append( atof( g.command( TE.at(jdx).toUtf8().constData() ).c_str() ));
    //                gVel.at(jdx)->append( atof( g.command( TV.at(jdx).toUtf8().constData() ).c_str() ));
    //                gTorque.at(jdx)->append( atof( g.command( TT.at(jdx).toUtf8().constData() ).c_str() ));
    //            }

    //            //qDebug()<< atof( g.command( "TL?" ).c_str() );

    //        }
    //    }



    updateLastMove();

    //plotMeasurements(false);

    //    if (saveLog){
    //        fMan.saveLogDialog(gPos, gVel, gPosErr, gTorque, TimeC, jointPosRefmm);
    //    }

    //qDebug()<<"End!!!!";
}


void MainWindow::loadGCode(){
    gCodeLoaded = fMan.loadGCode( &Kind_traj, &Roll, &Pitch, &Yaw, &X, &Y, &Z, &X_center, &Y_center, &Z_center, &Radio, &Velocity, &t );

    dispGCodeInfo( &Kind_traj, &Roll, &Pitch, &Yaw, &X, &Y, &Z, &X_center, &Y_center, &Z_center, &Radio, &Velocity, &t );

    for(int i=0; i<jointPosRefmm.size(); i++){
        jointPosRefmm.at(i)->clear();
        jointVelNumRefmm.at(i)->clear();
        jointAccNumRefmm.at(i)->clear();
        jointJerkNumRefmm.at(i)->clear();
    }

    pot_dos = this->ui->smplTimecomboBox->currentIndex()+1;
    double timeStep = pow(2,pot_dos)/1000.0;

    executeGcode = trajGen.genGCodeTraj( &Kind_traj, &Roll, &Pitch, &Yaw, &X, &Y, &Z, &X_center, &Y_center, &Z_center,
                                         &Radio, &Velocity, &t,timeStep, mysgp, jointPosRefmm);

    //rc->clearMeasurements();
    //this->plotMeasurementsSlot(true, rc->TimeC, rc->gPos, rc->gPosErr, rc->gVel, rc->gTorque);
    //clearMeasurements();
    //plotMeasurements(true);

    numericDeriv(jointPosRefmm, &jointVelNumRefmm, timeStep);

    for(int j=0; j<6; j++){
        jointPos.at(j)->plot(timeStep*1000, *(jointPosRefmm.at(j)));
        jointVel.at(j)->plot(timeStep*1000, *(jointVelNumRefmm.at(j)));
        jointAcc.at(j)->clear();
        jointJerk.at(j)->clear();
    }

    if (!executeGcode){
        QMessageBox msgBox;
        msgBox.setText("Motion won't be executed because actuators are out of range or robot gets into singular positions");
        msgBox.setWindowTitle("Warning");
        msgBox.exec();
    }
    //plotMeasurements(true);
}

void MainWindow::loadJointTraj(){
    // Loading trajectory
    executeTraj = fMan.loadTrajectory( &pos_mm, &vel_mmps, &acc_mmps2, &time_s );

    // Changing position reference from mm to pulses
    pos_pulses = mm2pulses( pos_mm );

    if ( pos_mm.size() > 0)
    {
        trajecLoaded = true;

        //-----------------------------
        for(int i=0; i<jointPosRefmm.size(); i++){
            jointPosRefmm.at(i)->clear();
            jointVelNumRefmm.at(i)->clear();
            jointAccNumRefmm.at(i)->clear();
            jointJerkNumRefmm.at(i)->clear();
        }

        //int pot_dos = 4; // n value for DT command
        //int interv = pow(2,pot_dos);

        pot_dos = ui->smplTimecomboBox->currentIndex()+1;
        interv = pow(2,pot_dos);


        for(int i=0; i <= pos_mm.size()-interv-1; i = i+interv)
            for(int j=0; j < jointPosRefmm.size(); j++ )
                jointPosRefmm.at(j)->append(pos_mm.at(i).at(j)-pos_mm.at(0).at(j));

        //rc->clearMeasurements();
        //this->plotMeasurementsSlot(true, rc->TimeC, rc->gPos, rc->gPosErr, rc->gVel, rc->gTorque);
        //clearMeasurements();
        //plotMeasurements(true);

        numericDeriv(jointPosRefmm, &jointVelNumRefmm, interv);

        for(int j=0; j<6; j++){
            jointPos.at(j)->plot(interv, *(jointPosRefmm.at(j)));
            jointVel.at(j)->plot(interv, *(jointVelNumRefmm.at(j)));
            jointAcc.at(j)->clear();
            jointJerk.at(j)->clear();
        }

        //-----------------------------
        QMessageBox msgBox;
        msgBox.setText("Trajectory loaded");
        msgBox.setWindowTitle("Notification");
        msgBox.exec();

        if (!executeTraj){
            QMessageBox msgBox1;
            msgBox1.setText("Trajectory won't be executed because actuators are out of range");
            msgBox1.setWindowTitle("Warnning");
            msgBox1.exec();
        }
    }else
    {
        qDebug() << "No trajectory has been loaded";
    }
}

void MainWindow::execGCode(){
    if(gCodeLoaded){
        if (!executeGcode){
            QMessageBox msgBox;
            msgBox.setText("Motion won't be executed because actuators are out of range or robot gets into singular positions");
            msgBox.setWindowTitle("Warning");
            msgBox.exec();
        }else{
            ui->xPosLineEdit->setText(QString::number(mysgp.x));
            ui->yPosLineEdit->setText(QString::number(mysgp.y));
            ui->zPosLineEdit->setText(QString::number(mysgp.z));

            rc->clearMeasurements();
            cleanMeasurementsGraph();
            //this->plotMeasurementsSlot(true, rc->TimeC, rc->gPos, rc->gPosErr, rc->gVel, rc->gTorque);

            rControlThread->start();
            //emit jMoveSignal(g, ui->smplTimecomboBox->currentIndex()+1, 0, jointPosRefmm );
            emit jMoveSignal(&g, ui->smplTimecomboBox->currentIndex()+1, 0, jointPosRefmm );
        }

    }else{
        QMessageBox msgBox;
        msgBox.setText("No GCode has been loaded");
        msgBox.setWindowTitle("Notification");
        msgBox.exec();
    }
}

void MainWindow::execJointTraj(){
    if(trajecLoaded){
        if (!executeTraj){
            QMessageBox msgBox1;
            msgBox1.setText("Trajectory won't be executed because actuators are out of range");
            msgBox1.setWindowTitle("Warnning");
            msgBox1.exec();
        }else{
            rc->clearMeasurements();
            cleanMeasurementsGraph();

            rControlThread->start();
            //emit jMoveSignal(g, ui->smplTimecomboBox->currentIndex()+1, 0, jointPosRefmm );
            emit jMoveSignal(&g, ui->smplTimecomboBox->currentIndex()+1, 0, jointPosRefmm );

            trajecLoaded = false;
            executeTraj = false;
        }
    }else{
        QMessageBox msgBox;
        msgBox.setText("No Trajectory has been loaded");
        msgBox.setWindowTitle("Notification");
        msgBox.exec();
    }
}

void MainWindow::execManualJoint(int axisId, QList< QList<double> > aux, double timeStep){

    qDebug()<<"From the ui thread: "<<QThread::currentThreadId();

    plotJoint(axisId, aux, timeStep*1000);

    rc->clearMeasurements();
    cleanMeasurementsGraph();

    rControlThread->start();
    //emit jMoveSignal(g, ui->smplTimecomboBox->currentIndex()+1, axisId, jointPosRefmm );
    emit jMoveSignal(&g, ui->smplTimecomboBox->currentIndex()+1, axisId, jointPosRefmm );
}

void MainWindow::execManualWSpace(QString wsAxisId, double direction, double wSpaceStep, bool execute, double timeStep,
                                  bool cyclic){

    numericDeriv(jointPosRefmm, &jointVelNumRefmm, timeStep);

    for(int j=0; j<6; j++){
        jointPos.at(j)->plot(timeStep*1000, *(jointPosRefmm.at(j)));
        jointVel.at(j)->plot(timeStep*1000, *(jointVelNumRefmm.at(j)));
        jointAcc.at(j)->clear();
        jointJerk.at(j)->clear();

        //jointAcc.at(j)->plot(timeStep*1000, *(jointAccNumRefmm.at(j)));
        //jointJerk.at(j)->plot(timeStep*1000, *(jointJerkNumRefmm.at(j)));
    }

    rc->clearMeasurements();
    cleanMeasurementsGraph();

    if (!execute){
        QMessageBox msgBox;
        msgBox.setText("Motion won't be executed because actuators would get negative positions");
        msgBox.setWindowTitle("Warning");
        msgBox.exec();
    }else{
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText("Do you really wanna execute?");
        msgBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        if(msgBox.exec() == QMessageBox::Yes){
            if (!cyclic){
                if(wsAxisId == "X"){
                    mysgp.setPose(mysgp.x + direction*wSpaceStep,  mysgp.y, mysgp.z, mysgp.roll, mysgp.pitch, mysgp.yaw);
                }else if(wsAxisId == "Y"){
                    mysgp.setPose(mysgp.x,  mysgp.y + direction*wSpaceStep, mysgp.z, mysgp.roll, mysgp.pitch, mysgp.yaw);
                }else if(wsAxisId == "Z"){
                    mysgp.setPose(mysgp.x,  mysgp.y, mysgp.z + direction*wSpaceStep, mysgp.roll, mysgp.pitch, mysgp.yaw);
                }else if(wsAxisId == "R"){
                    mysgp.setPose(mysgp.x,  mysgp.y, mysgp.z, mysgp.roll + direction*wSpaceStep, mysgp.pitch, mysgp.yaw);
                }else if(wsAxisId == "P"){
                    mysgp.setPose(mysgp.x,  mysgp.y, mysgp.z, mysgp.roll, mysgp.pitch + direction*wSpaceStep, mysgp.yaw);
                }else if(wsAxisId == "YZ"){
                    mysgp.setPose(mysgp.x,  mysgp.y, mysgp.z, mysgp.roll, mysgp.pitch, mysgp.yaw + direction*wSpaceStep);
                }
            }

            ui->xPosLineEdit->setText(QString::number(mysgp.x));
            ui->yPosLineEdit->setText(QString::number(mysgp.y));
            ui->zPosLineEdit->setText(QString::number(mysgp.z));
            ui->rollPosLineEdit->setText(QString::number(mysgp.roll));
            ui->pitchPosLineEdit->setText(QString::number(mysgp.pitch));
            ui->yawPosLineEdit->setText(QString::number(mysgp.yaw));


            rControlThread->start();
            //emit jMoveSignal(g, ui->smplTimecomboBox->currentIndex()+1, 0, jointPosRefmm );
            emit jMoveSignal(&g, ui->smplTimecomboBox->currentIndex()+1, 0, jointPosRefmm );
        }else{
            qDebug() << "Menos mal";
        }
    }
}

void MainWindow::on_actionLoad_G_Code_triggered(){ loadGCode(); }

void MainWindow::on_actionLoad_Joint_Trajectory_triggered(){ loadJointTraj();}

void MainWindow::on_execGCodeButton_clicked(){ execGCode(); }

void MainWindow::on_execJointTrajButton_clicked(){ execJointTraj(); }

void MainWindow::on_actionExecute_G_Code_triggered(){ execGCode(); }

void MainWindow::on_actionExecute_Joint_Trajectory_triggered(){ execJointTraj(); }

void MainWindow::on_actionInfo_triggered()
{
    //   qDebug() << QString(g.connection().data()); //Print connection string
    //   qDebug() << QString(g.libraryVersion().data()); //Print Library Version
    //   qDebug() << "MG TIME " << QString(g.command("MG TIME").data()) ; //Send MG TIME to controller and get response

}

void MainWindow::nAxisMove(int axisId, double direction){

    qDebug()<<"From the buttom thread: "<<QThread::currentThreadId();

    trajGen.nAxisMove(axisId, direction, ui->stepSizeJointLineEdit->text().toDouble(),
                      ui->smplTimecomboBox->currentIndex()+1, interpolationType,
                      ui->timeIntervalJointLineEdit->text().toDouble(), ui->maxJerkLineEdit->text().toDouble(),
                      jointPosRefmm, jointVelNumRefmm, jointAccNumRefmm, jointJerkNumRefmm );

}

void MainWindow::nAxisMoveN(int axisId, int nTimes, double jointStep){
    trajGen.nAxisMoveN(axisId, nTimes, jointStep,
                       ui->smplTimecomboBox->currentIndex()+1, interpolationType,
                       ui->timeIntervalJointLineEdit->text().toDouble(), ui->maxJerkLineEdit->text().toDouble(),
                       jointPosRefmm, jointVelNumRefmm, jointAccNumRefmm, jointJerkNumRefmm);
}

void MainWindow::on_aPlusPushButton_clicked(){  nAxisMove(1, 1.0);  }

void MainWindow::on_aMinusPushButton_clicked(){ nAxisMove(1, -1.0); }

void MainWindow::on_bPlusPushButton_clicked(){  nAxisMove(2, 1.0);  }

void MainWindow::on_bMinusPushButton_clicked(){ nAxisMove(2, -1.0); }

void MainWindow::on_cPlusPushButton_clicked(){  nAxisMove(3, 1.0);  }

void MainWindow::on_cMinusPushButton_clicked(){ nAxisMove(3, -1.0); }

void MainWindow::on_dPlusPushButton_clicked(){  nAxisMove(4, 1.0);  }

void MainWindow::on_dMinusPushButton_clicked(){ nAxisMove(4, -1.0); }

void MainWindow::on_ePlusPushButton_clicked(){  nAxisMove(5, 1.0);  }

void MainWindow::on_eMinusPushButton_clicked(){ nAxisMove(5, -1.0); }

void MainWindow::on_fPlusPushButton_clicked(){  nAxisMove(6, 1.0);  }

void MainWindow::on_fMinusPushButton_clicked(){ nAxisMove(6, -1.0); }

void MainWindow::on_servoOffButton_clicked(){ /* servoOff(&g);  */}

void MainWindow::on_servoOnButton_clicked(){    /*servoOn(&g);  */ }

void MainWindow::on_startJoggingButton_clicked(){
    nAxisMoveN(this->ui->axisIdComboBox->currentIndex()+1,
               this->ui->nJogComboBox->currentText().toInt(),
               this->ui->stepSizeJointLineEdit->text().toDouble());
}

void MainWindow::moveWSpace(QString wsAxisId, double direction){    
    trajGen.moveWSpace(wsAxisId, direction, ui->stepSizeJointLineEdit->text().toDouble(),
                       ui->smplTimecomboBox->currentIndex()+1, interpolationType,
                       ui->timeIntervalJointLineEdit->text().toDouble(),
                       ui->maxJerkLineEdit->text().toDouble(),
                       jointPosRefmm, jointVelNumRefmm, jointAccNumRefmm, jointJerkNumRefmm, mysgp );
}

void MainWindow::moveWSpaceN(QString wsAxisId, int nTimes){
    trajGen.moveWSpaceN(wsAxisId, nTimes, ui->stepSizeJointLineEdit->text().toDouble(),
                        ui->smplTimecomboBox->currentIndex()+1, interpolationType,
                        ui->timeIntervalJointLineEdit->text().toDouble(),
                        ui->maxJerkLineEdit->text().toDouble(),
                        jointPosRefmm, jointVelNumRefmm, jointAccNumRefmm, jointJerkNumRefmm, mysgp);
}

void MainWindow::rotWSpace(QString rotAxisId, double direction){
    trajGen.rotWSpace(rotAxisId, direction, ui->stepSizeGradJointLineEdit->text().toDouble(),
                      ui->smplTimecomboBox->currentIndex()+1, interpolationType,
                      ui->timeIntervalJointLineEdit->text().toDouble(),
                      ui->maxJerkLineEdit->text().toDouble(),
                      jointPosRefmm, jointVelNumRefmm, jointAccNumRefmm, jointJerkNumRefmm, mysgp );
}

void MainWindow::rotWSpaceN(QString rotAxisId, int nTimes){
    trajGen.rotWSpaceN(rotAxisId, nTimes, ui->stepSizeGradJointLineEdit->text().toDouble(),
                       ui->smplTimecomboBox->currentIndex()+1, interpolationType,
                       ui->timeIntervalJointLineEdit->text().toDouble(),
                       ui->maxJerkLineEdit->text().toDouble(),
                       jointPosRefmm, jointVelNumRefmm, jointAccNumRefmm, jointJerkNumRefmm, mysgp );
}

void MainWindow::on_checkBox1_toggled(bool checked){ checked ? jointPos.at(0)->attach() : jointPos.at(0)->dettach(); }

void MainWindow::on_checkBox2_toggled(bool checked){ checked ? jointPos.at(1)->attach() : jointPos.at(1)->dettach(); }

void MainWindow::on_checkBox3_toggled(bool checked){ checked ? jointPos.at(2)->attach() : jointPos.at(2)->dettach(); }

void MainWindow::on_checkBox4_toggled(bool checked){ checked ? jointPos.at(3)->attach() : jointPos.at(3)->dettach(); }

void MainWindow::on_checkBox5_toggled(bool checked){ checked ? jointPos.at(4)->attach() : jointPos.at(4)->dettach(); }

void MainWindow::on_checkBox6_toggled(bool checked){ checked ? jointPos.at(5)->attach() : jointPos.at(5)->dettach(); }

void MainWindow::on_checkBox1_2_toggled(bool checked){ checked ? jointVel.at(0)->attach() : jointVel.at(0)->dettach(); }

void MainWindow::on_checkBox2_2_toggled(bool checked){ checked ? jointVel.at(1)->attach() : jointVel.at(1)->dettach(); }

void MainWindow::on_checkBox3_2_toggled(bool checked){ checked ? jointVel.at(2)->attach() : jointVel.at(2)->dettach(); }

void MainWindow::on_checkBox4_2_toggled(bool checked){ checked ? jointVel.at(3)->attach() : jointVel.at(3)->dettach(); }

void MainWindow::on_checkBox5_2_toggled(bool checked){ checked ? jointVel.at(4)->attach() : jointVel.at(4)->dettach(); }

void MainWindow::on_checkBox6_2_toggled(bool checked){ checked ? jointVel.at(5)->attach() : jointVel.at(5)->dettach(); }

void MainWindow::on_checkBox1_4_toggled(bool checked){ checked ? jointAcc.at(0)->attach() : jointAcc.at(0)->dettach(); }

void MainWindow::on_checkBox2_4_toggled(bool checked){ checked ? jointAcc.at(1)->attach() : jointAcc.at(1)->dettach(); }

void MainWindow::on_checkBox3_4_toggled(bool checked){ checked ? jointAcc.at(2)->attach() : jointAcc.at(2)->dettach(); }

void MainWindow::on_checkBox4_4_toggled(bool checked){ checked ? jointAcc.at(3)->attach() : jointAcc.at(3)->dettach(); }

void MainWindow::on_checkBox5_4_toggled(bool checked){ checked ? jointAcc.at(4)->attach() : jointAcc.at(4)->dettach(); }

void MainWindow::on_checkBox6_4_toggled(bool checked){ checked ? jointAcc.at(5)->attach() : jointAcc.at(5)->dettach(); }

void MainWindow::on_checkBox1_5_toggled(bool checked){ checked ? jointJerk.at(0)->attach() : jointJerk.at(0)->dettach(); }

void MainWindow::on_checkBox2_5_toggled(bool checked){ checked ? jointJerk.at(1)->attach() : jointJerk.at(1)->dettach(); }

void MainWindow::on_checkBox3_5_toggled(bool checked){ checked ? jointJerk.at(2)->attach() : jointJerk.at(2)->dettach(); }

void MainWindow::on_checkBox4_5_toggled(bool checked){ checked ? jointJerk.at(3)->attach() : jointJerk.at(3)->dettach(); }

void MainWindow::on_checkBox5_5_toggled(bool checked){ checked ? jointJerk.at(4)->attach() : jointJerk.at(4)->dettach(); }

void MainWindow::on_checkBox6_5_toggled(bool checked){ checked ? jointJerk.at(5)->attach() : jointJerk.at(5)->dettach(); }

void MainWindow::on_checkBox1_6_toggled(bool checked){ checked ? jointErrPos.at(0)->attach() : jointErrPos.at(0)->dettach(); }

void MainWindow::on_checkBox2_6_toggled(bool checked){ checked ? jointErrPos.at(1)->attach() : jointErrPos.at(1)->dettach(); }

void MainWindow::on_checkBox3_6_toggled(bool checked){ checked ? jointErrPos.at(2)->attach() : jointErrPos.at(2)->dettach(); }

void MainWindow::on_checkBox4_6_toggled(bool checked){ checked ? jointErrPos.at(3)->attach() : jointErrPos.at(3)->dettach(); }

void MainWindow::on_checkBox5_6_toggled(bool checked){ checked ? jointErrPos.at(4)->attach() : jointErrPos.at(4)->dettach(); }

void MainWindow::on_checkBox6_6_toggled(bool checked){ checked ? jointErrPos.at(5)->attach() : jointErrPos.at(5)->dettach(); }

void MainWindow::on_checkBox1_3_toggled(bool checked){ checked ? jointMeasuredPos1.attach() : jointMeasuredPos1.dettach(); }

void MainWindow::on_checkBox2_3_toggled(bool checked){ checked ? jointMeasuredPos2.attach() : jointMeasuredPos2.dettach(); }

void MainWindow::on_checkBox3_3_toggled(bool checked){ checked ? jointMeasuredPos3.attach() : jointMeasuredPos3.dettach(); }

void MainWindow::on_checkBox4_3_toggled(bool checked){ checked ? jointMeasuredPos4.attach() : jointMeasuredPos4.dettach(); }

void MainWindow::on_checkBox5_3_toggled(bool checked){ checked ? jointMeasuredPos5.attach() : jointMeasuredPos5.dettach(); }

void MainWindow::on_checkBox6_3_toggled(bool checked){ checked ? jointMeasuredPos6.attach() : jointMeasuredPos6.dettach(); }

void MainWindow::on_checkBox1_8_toggled(bool checked){ checked ? jointMeasuredVel1.attach() : jointMeasuredVel1.dettach(); }

void MainWindow::on_checkBox2_8_toggled(bool checked){ checked ? jointMeasuredVel2.attach() : jointMeasuredVel2.dettach(); }

void MainWindow::on_checkBox3_8_toggled(bool checked){ checked ? jointMeasuredVel3.attach() : jointMeasuredVel3.dettach(); }

void MainWindow::on_checkBox4_8_toggled(bool checked){ checked ? jointMeasuredVel4.attach() : jointMeasuredVel4.dettach(); }

void MainWindow::on_checkBox5_8_toggled(bool checked){ checked ? jointMeasuredVel5.attach() : jointMeasuredVel5.dettach(); }

void MainWindow::on_checkBox6_8_toggled(bool checked){ checked ? jointMeasuredVel6.attach() : jointMeasuredVel6.dettach(); }

void MainWindow::on_checkBox1_7_toggled(bool checked){ checked ? jointMeasuredTorque1.attach() : jointMeasuredTorque1.dettach(); }

void MainWindow::on_checkBox2_7_toggled(bool checked){ checked ? jointMeasuredTorque2.attach() : jointMeasuredTorque2.dettach(); }

void MainWindow::on_checkBox3_7_toggled(bool checked){ checked ? jointMeasuredTorque3.attach() : jointMeasuredTorque3.dettach(); }

void MainWindow::on_checkBox4_7_toggled(bool checked){ checked ? jointMeasuredTorque4.attach() : jointMeasuredTorque4.dettach(); }

void MainWindow::on_checkBox5_7_toggled(bool checked){ checked ? jointMeasuredTorque5.attach() : jointMeasuredTorque5.dettach(); }

void MainWindow::on_checkBox6_7_toggled(bool checked){ checked ? jointMeasuredTorque6.attach() : jointMeasuredTorque6.dettach(); }

//-----------------------------------

void MainWindow::on_xPlusPushButton_clicked(){ moveWSpace("X", 1); }

void MainWindow::on_xMinusPushButton_clicked(){ moveWSpace("X", -1); }

void MainWindow::on_yPlusPushButton_clicked(){ moveWSpace("Y", 1); }

void MainWindow::on_yMinusPushButton_clicked(){ moveWSpace("Y", -1); }

void MainWindow::on_zPlusPushButton_clicked(){ moveWSpace("Z", 1); }

void MainWindow::on_zMinusPushButton_clicked(){ moveWSpace("Z", -1); }

void MainWindow::on_startJoggingButton_2_clicked(){
    QString axisIdJog = this->ui->axisIdWSComboBox->currentText().toUpper();
    if (axisIdJog == "X" || axisIdJog == "Y" || axisIdJog == "Z"){
        moveWSpaceN(axisIdJog, this->ui->nWSJogComboBox->currentIndex()+1);
    }else if (axisIdJog == "R" || axisIdJog == "P" || axisIdJog == "YZ"){
        rotWSpaceN(axisIdJog, this->ui->nWSJogComboBox->currentIndex()+1);
    }
}

void MainWindow::on_rollPlusPushButton_clicked(){ rotWSpace("R", 1); }

void MainWindow::on_rollMinusPushButton_clicked(){ rotWSpace("R", -1); }

void MainWindow::on_pitchPlusPushButton_clicked(){ rotWSpace("P", 1); }

void MainWindow::on_pitchMinusPushButton_clicked(){ rotWSpace("P", -1); }

void MainWindow::on_actionLog_Folder_triggered(bool checked){ saveLog = checked; }

void MainWindow::on_actionSG_parameters_triggered(){
    rp.exec();
    mysgp.setConfiguration( rp.diaBase/2, rp.A1/2, rp.diaMobile/2, rp.A2/2, rp.L1, rp.L2);
}

void MainWindow::updateGuiPos(){
    ui->xPosLineEdit->setText(QString::number(mysgp.x));
    ui->yPosLineEdit->setText(QString::number(mysgp.y));
    ui->zPosLineEdit->setText(QString::number(mysgp.z));
    ui->rollPosLineEdit->setText(QString::number(mysgp.roll));
    ui->pitchPosLineEdit->setText(QString::number(mysgp.pitch));
    ui->yawPosLineEdit->setText(QString::number(mysgp.yaw));
}

void MainWindow::on_actionSet_Position_triggered(){
    pp.refreshPose(mysgp.x, mysgp.y, mysgp.z, mysgp.roll, mysgp.pitch, mysgp.yaw );
    pp.exec();

    mysgp.setPose(pp.x, pp.y, pp.z, pp.roll, pp.pitch, pp.yaw);
    updateGuiPos();
}

void MainWindow::checkTimes(){
    double timeInte = this->ui->timeIntervalJointLineEdit->text().toDouble();
    double smplTime = this->ui->smplTimecomboBox->currentText().toDouble();
    if(qFabs(fmod(timeInte, smplTime)) > 0.001){
        QMessageBox msgBox;
        msgBox.setText("Time interval should be an integer multiple of the sample time");
        msgBox.setWindowTitle("Warning");
        msgBox.exec();
        this->ui->timeIntervalJointLineEdit->setText( QString::number(((int)(timeInte/smplTime))*smplTime) );
    }
}

void MainWindow::on_timeIntervalJointLineEdit_editingFinished(){ checkTimes(); }

void MainWindow::on_smplTimecomboBox_currentIndexChanged(int index){ checkTimes(); }

void MainWindow::on_actionQuintic_triggered()
{
    ui->actionQuintic->setChecked(true);
    ui->actionDouble_S->setChecked(false);

    ui->timeVelLabel->setText("Time interval:");
    ui->timeIntervalJointLineEdit->setText("3000.0");
    ui->timeVelUnitLabel->setText("ms");
    ui->timeVelUnitLabel->update();
    ui->trajParam_groupBox->setFixedHeight(140);

    ui->maxJerkLabel->setVisible(false);
    ui->maxJerkLineEdit->setVisible(false);
    ui->maxJerkUnitLabel->setVisible(false);

    interpolationType = 1;
}

void MainWindow::on_actionDouble_S_triggered()
{
    ui->actionQuintic->setChecked(false);
    ui->actionDouble_S->setChecked(true);

    ui->timeVelLabel->setText("Cruise velocity:");
    ui->timeIntervalJointLineEdit->setText("50.0");
    ui->timeVelUnitLabel->setText("mm/s");
    ui->trajParam_groupBox->setFixedHeight(180);

    ui->maxJerkLabel->setVisible(true);
    ui->maxJerkLineEdit->setVisible(true);
    ui->maxJerkUnitLabel->setVisible(true);

    interpolationType = 2;
}

