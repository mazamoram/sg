#include "robotcontroller.h"

RobotController::RobotController()
{
    this->Buffer = 510;
    this->Bloque = 100;
    this->TP << QString("TPA") << QString("TPB") << QString("TPC") << QString("TPD") << QString("TPE") << QString("TPF");
    this->TE << QString("MG_TEA") << QString("MG_TEB") << QString("MG_TEC") << QString("MG_TED") << QString("MG_TEE") << QString("MG_TEF");
    this->TV << QString("MG_TVA") << QString("MG_TVB") << QString("MG_TVC") << QString("MG_TVD") << QString("MG_TVE") << QString("MG_TVF");
    this->TT << QString("MG_TTA") << QString("MG_TTB") << QString("MG_TTC") << QString("MG_TTD") << QString("MG_TTE") << QString("MG_TTF");

    gPos << &gPosA << &gPosB << &gPosC << &gPosD << &gPosE << &gPosF;
    gPosErr <<  &gPosErrA <<  &gPosErrB <<  &gPosErrC <<  &gPosErrD <<  &gPosErrE <<  &gPosErrF;
    gVel << &gVelA << &gVelB << &gVelC << &gVelD << &gVelE << &gVelF;
    gTorque << &gTorqueA << &gTorqueB << &gTorqueC << &gTorqueD << &gTorqueE << &gTorqueF;
}

RobotController::~RobotController()
{

}

void RobotController::clearMeasurements()
{
    this->TimeC.clear();
    for( int i=0; i < gPos.size(); i++ )
    {
        this->gPos.at(i)->clear();
        this->gPosErr.at(i)->clear();
        this->gVel.at(i)->clear();
        this->gTorque.at(i)->clear();
    }
}

QString RobotController::createCD(int axisId, QList <QList<double>* > jointPosRefmm, int i)
{
    if(axisId == 0){
        return QString("CD") +
                QString::number( mm2pulses( jointPosRefmm.at(0)->at(i)-jointPosRefmm.at(0)->at(i-1) ) ) + QString(",") +
                QString::number( mm2pulses( jointPosRefmm.at(1)->at(i)-jointPosRefmm.at(1)->at(i-1) ) ) + QString(",") +
                QString::number( mm2pulses( jointPosRefmm.at(2)->at(i)-jointPosRefmm.at(2)->at(i-1) ) ) + QString(",") +
                QString::number( mm2pulses( jointPosRefmm.at(3)->at(i)-jointPosRefmm.at(3)->at(i-1) ) ) + QString(",") +
                QString::number( mm2pulses( jointPosRefmm.at(4)->at(i)-jointPosRefmm.at(4)->at(i-1) ) ) + QString(",") +
                QString::number( mm2pulses( jointPosRefmm.at(5)->at(i)-jointPosRefmm.at(5)->at(i-1) ) );
    }
    else if(axisId == 1){
        return QString("CD") + QString::number( mm2pulses( jointPosRefmm.at(axisId-1)->at(i) - jointPosRefmm.at(axisId-1)->at(i-1) ) ) +
                QString(",0,0,0,0,0");
    }else if(axisId == 2){
        return QString("CD0,") + QString::number( mm2pulses( jointPosRefmm.at(axisId-1)->at(i) - jointPosRefmm.at(axisId-1)->at(i-1) ) ) +
                QString(",0,0,0,0");
    }else if(axisId == 3){
        return QString("CD0,0,")+ QString::number( mm2pulses( jointPosRefmm.at(axisId-1)->at(i) - jointPosRefmm.at(axisId-1)->at(i-1) ) ) +
                QString(",0,0,0");
    }else if(axisId == 4){
        return QString("CD0,0,0,")+ QString::number( mm2pulses( jointPosRefmm.at(axisId-1)->at(i)-jointPosRefmm.at(axisId-1)->at(i-1) ) ) +
                QString(",0,0");
    }else if(axisId == 5){
        return QString("CD0,0,0,0,")+ QString::number( mm2pulses( jointPosRefmm.at(axisId-1)->at(i)-jointPosRefmm.at(axisId-1)->at(i-1) ) ) +
                QString(",0");
    }else if(axisId == 6){
        return QString("CD0,0,0,0,0,") + QString::number( mm2pulses( jointPosRefmm.at(axisId-1)->at(i)-jointPosRefmm.at(axisId-1)->at(i-1) ) );
    }else{
        return QString("CD0,0,0,") + QString("0,0,0") ;
    }
}


void RobotController::readSensors(Galil *g){
    this->TimeC.append(  atof( g->command("MG TIME").c_str() )  );
    for(int jdx = 0; jdx < this->gPos.size(); jdx++){
        this->gPos.at(jdx)->append(    atof( g->command( this->TP.at(jdx).toUtf8().constData() ).c_str() ) );
        this->gPosErr.at(jdx)->append( atof( g->command( this->TE.at(jdx).toUtf8().constData() ).c_str() ) );
        this->gVel.at(jdx)->append(    atof( g->command( this->TV.at(jdx).toUtf8().constData() ).c_str() ) );
        this->gTorque.at(jdx)->append( atof( g->command( this->TT.at(jdx).toUtf8().constData() ).c_str() ) );
    }
}

void RobotController::sendBlock(Galil *g, int lowerIx, int upperIx, QList < QList<double>* > jointPosRefmm,
                                int axisId, QString p){

    for (int ix = lowerIx; ix<upperIx; ix++){
        this->CD = createCD(axisId, jointPosRefmm, ix);    //qDebug()<< this->CD;
        g->command(this->CD.toUtf8().constData());         //qDebug() << p << ":" << this->CD.toUtf8().constData();
    }
    //if(motionEnable &&  this->cntr < upperIx){
    //        this->CD = createCD(axisId, jointPosRefmm, this->cntr++); //qDebug()<< this->CD;
    //        g->command(this->CD.toUtf8().constData());

    //        //qDebug() << p << ":" << this->CD.toUtf8().constData();
    //        emit callSendBlock(g, lowerIx, upperIx, jointPosRefmm, axisId, p,
    //                           TimeC, gPos, gPosErr, gVel, gTorque);
    //}
    /*else{
        if (this->cntr == jointPosRefmm.at(this->axisIdAux-1)->size()){
            //g->command("CD 0,0,0,0,0,0=0"); //indica el punto final del contorno
        }
        if(lowerIx == 1 && this->cntr == upperIx){
            emit callJMoveLoopSlot(g, jointPosRefmm, TimeC, gPos, gPosErr, gVel, gTorque);
        }
        //qDebug()<<"weard call: "<< ++weirdCntr;
    }*/
}

void RobotController::jMoveLoopSlot(Galil *g, QList < QList<double>* > jointPosRefmm ){

    sendBlock(g, 1, this->Bloque,  jointPosRefmm, this->axisId, "p0");

    //qDebug()<<"Hola, llegue a jMoveLoopSlot";
    //if(this->motionEnable &&  this->Buffer != 511 ){
    while(Buffer != 511){
        this->Buffer = atoi( g->command("CM?").c_str() ) ;  //qDebug()<<"-----------"<<Buffer;
        if ( this->Buffer > 150 && this->Buffer < 500 && contador ==1){
            if ((jointPosRefmm.at(this->axisIdAux-1)->size() - this->Bloque) < 100){
                sendBlock(g, this->Bloque, jointPosRefmm.at(this->axisIdAux-1)->size(),  jointPosRefmm,
                          this->axisId, "p1");
                contador = 2;
                g->command("CD 0,0,0,0,0,0=0"); //indica el punto final del contorno
            }else{
                //sendBlock(g, this->Bloque, this->Bloque + this->Buffer-20,  jointPosRefmm, this->axisId, "p2");
                //this->Bloque = this->Bloque + this->Buffer-20;
                sendBlock(g, this->Bloque, this->Bloque + 100,  jointPosRefmm, this->axisId, "p2");
                this->Bloque = this->Bloque + 100;
            }           
        }
        readSensors(g);
        emit updatePlotSignal(true, TimeC, gPos, gPosErr, gVel, gTorque);
    }
    readSensors(g);
    emit updatePlotSignal(true, TimeC, gPos, gPosErr, gVel, gTorque);
    emit finish();
    // Luego de que termine el contorno
    // el comando CM permite limpiar el buffer siempre y cuando los motores se
    // encuentren encendidos

    //    while(Buffer != 511){
    //        Buffer = atoi( g.command("CM?").c_str() ) ;  //qDebug()<<"-----------"<<Buffer;
    //        if ( Buffer > 150 && Buffer < 500 && contador ==1){
    //            if (jointPosRefmm.at(axisIdAux-1)->size() - Bloque< 100){
    //                this->sendBlock(g, Bloque, jointPosRefmm.at(axisIdAux-1)->size(),  jointPosRefmm, axisId, "p1");
    //                contador = 2;
    //                g.command("CD 0,0,0,0,0,0=0"); //indica el punto final del contorno
    //            }else{
    //                this->sendBlock(g, Bloque, Bloque+100,  jointPosRefmm, axisId, "p2");
    //                Bloque = Bloque + 100;
    //            }
    //        }
    //        this->readSensors(g, TimeC, gPos,  gPosErr, gVel, gTorque);
    //        //emit update
    //    }
    //    this->readSensors(g, TimeC, gPos,  gPosErr, gVel, gTorque);
    //    //emit update.
}

void RobotController::jMoveSlot(Galil *g,  int pot_dos, int axisId, QList < QList<double>* > jointPosRefmm){

    qDebug()<<"From the rc controller thread: "<<QThread::currentThreadId();

    this->axisId = axisId;
    this->axisIdAux = axisId;
    this->contador = 1;

    this->Bloque = 100;
    this->Buffer = 510;

    this->clearMeasurements();

    QString pot_dos_str = QString::number(pot_dos);
    QString DTN = QString("DT ")+ pot_dos_str;
    g->command("SHABCDEF");
    g->command("CMABCDEF"); //g.command("CMF");
    qDebug()<<"Se limpio? "<<atoi( g->command("CM?").c_str() );
    g->command("CMABCDEF");
    g->command(DTN.toUtf8().constData()); //qDebug()<< DTN.toUtf8().constData();

    if(axisId==0){ this->axisIdAux = 1; }

    if (Bloque > jointPosRefmm.at(this->axisIdAux-1)->size()){
        Bloque = jointPosRefmm.at(this->axisIdAux-1)->size();
        contador = 2;
    }

    //--------------------------------------

    //this->myTimer.start();
    readSensors(g);
    emit updatePlotSignal(true, TimeC, gPos, gPosErr, gVel, gTorque);

    //--------------------------------------

    jMoveLoopSlot(g, jointPosRefmm);

    //--------------------------------------
    emit saveLog( TimeC, gPos, gPosErr, gVel, gTorque, jointPosRefmm );
    //emit finish();

    //if (saveLog){ saveLogDialog(gPos, gVel, gPosErr, gTorque, TimeC, jointPosRefmm); }
    //updateLastMove();

    //plotMeasurements(false);
    //qDebug()<<"End!!!!";

    //--------------------------------------
}
