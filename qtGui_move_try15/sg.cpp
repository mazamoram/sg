#include "sg.h"
using namespace arma;
using namespace std;

sg::sg()
{

}

sg::sg( double RadBase, double A1, double RadMobile, double A2,  double L1, double L2)
{
    this->RadBase = RadBase;
    this->A1 = A1;
    this->RadMobile = RadMobile;
    this->A2 = A2;
    this->L1 = L1;
    this->L2 = L2;    
    QList<double> tmpAnglBase;
    QList<double> tmpAnglMobile;

    //tmpAnglBase   << 90 - A1 << 330 + A1 << 330 - A1 << 210 + A1 << 210 - A1 << 90 + A1;
    tmpAnglBase   << A1 << 120 - A1 << 120 + A1 << 240 -A1 << 240 + A1 << 360 - A1;

    //tmpAnglMobile << 30 + A2 << 30 - A2 << 270 + A2 << 270 - A2 << 150 + A2 << 150 - A2;
    tmpAnglMobile << 60 - A2 << 60 + A2 << 180 - A2 << 180 + A2 << 300 - A2 << 300 + A2;

    for(int i = 0; i<6;i++)
    {
        this->ai << QVector3D( sin(qDegreesToRadians(tmpAnglBase.at(i))) * RadBase,
                               cos(qDegreesToRadians(tmpAnglBase.at(i))) * RadBase,
                               L1);
        this->bi << QVector3D( sin(qDegreesToRadians(tmpAnglMobile.at(i))) * RadMobile,
                               cos(qDegreesToRadians(tmpAnglMobile.at(i))) * RadMobile,
                               -L2);

//        this->ai << QVector3D( cos(qDegreesToRadians(tmpAnglBase.at(i))) * RadBase,
//                               sin(qDegreesToRadians(tmpAnglBase.at(i))) * RadBase,
//                               L1);
//        this->bi << QVector3D( cos(qDegreesToRadians(tmpAnglMobile.at(i))) * RadMobile,
//                               sin(qDegreesToRadians(tmpAnglMobile.at(i))) * RadMobile,
//                               -L2);
    }


    qDebug()<<"tmpAnglBase: "<< tmpAnglBase;
    qDebug()<<"tmpAnglMobile: "<< tmpAnglMobile;
    qDebug()<<"ai: "<<this->ai;
    qDebug()<<"bi: "<<this->ai;

}

sg::~sg()
{

}

QList<QVector3D> sg::invk(QVector3D p, double roll, double pitch, double yaw)
{
    QMatrix4x4 aRb;
//    QQuaternion qaRb = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), roll) *
//                      QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), pitch) *
//                      QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), yaw);
//    QQuaternion qaRb = QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), roll) *
//                      QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), pitch) *
//                      QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), yaw);
    QQuaternion qaRb = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), yaw) *
                       QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), pitch) *
                       QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), roll);

    aRb.rotate(qaRb);
    QList<QVector3D> Si;
    for (int i =0; i<6; i++){
        Si << p + aRb*bi.at(i) - ai.at(i);

    }
    //    qDebug()<<"d: "<<Si.at(i).length() - 1246.76 + 0.00438477;
    //    qDebug()<< "Si: ";
    //    qDebug()<< Si;
    return Si;
}


void sg::setConfiguration( double RadBase, double A1, double RadMobile, double A2,  double L1, double L2)
{
    this->RadBase = RadBase;
    this->A1 = A1;
    this->RadMobile = RadMobile;
    this->A2 = A2;
    this->L1 = L1;
    this->L2 = L2;
    QList<double> tmpAnglBase;
    QList<double> tmpAnglMobile;
    //tmpAnglBase   << 90 - A1 << 330 + A1 << 330 - A1 << 210 + A1 << 210 - A1 << 90 + A1;
    tmpAnglBase   << A1 << 120 - A1 << 120 + A1 << 240 -A1 << 240 + A1 << 360 - A1;

    //tmpAnglMobile << 30 + A2 << 30 - A2 << 270 + A2 << 270 - A2 << 150 + A2 << 150 - A2;
    tmpAnglMobile << 60 - A2 << 60 + A2 << 180 - A2 << 180 + A2 << 300 - A2 << 300 + A2;


    for(int i = 0; i<6;i++)
    {
        this->ai << QVector3D( sin(qDegreesToRadians(tmpAnglBase.at(i))) * RadBase,
                               cos(qDegreesToRadians(tmpAnglBase.at(i))) * RadBase,
                               L1);
        this->bi << QVector3D( sin(qDegreesToRadians(tmpAnglMobile.at(i))) * RadMobile,
                               cos(qDegreesToRadians(tmpAnglMobile.at(i))) * RadMobile,
                               -L2);
    }

    qDebug()<<"tmpAnglBase: "<< tmpAnglBase;
    qDebug()<<"tmpAnglMobile: "<< tmpAnglMobile;
    qDebug()<<"ai: "<<this->ai;
    qDebug()<<"bi: "<<this->ai;
}

QList<double> sg::getConfiguration()
{
    QList<double> aux;
    aux << this->RadBase << this->A1 << this->RadMobile << this->A2 << this->L1 << this->L2;
    return aux;
}

void sg::setPose(float x, float y, float z, float roll, float pitch, float yaw)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->roll = roll;
    this->pitch = pitch;
    this->yaw = yaw;
}

QList<double> sg::getPose()
{
    QList<double> aux;
    aux << this->x << this->y << this->z << this->roll << this->pitch << this->yaw;
    return aux;
}


mat sg::jacob(QList<QVector3D> Si)
{
    QList<double> aux;
    aux << this->x << this->y << this->z << this->roll << this->pitch << this->yaw;

    //    QQuaternion qaRb = QQuaternion::fromAxisAndAngle(QVector3D(1,1,0), 90);
    //    QMatrix4x4 aRb;
    //    aRb.rotate(qaRb);
    //    QVector3D Si = qaRb.rotatedVector( QVector3D (1,0,0 ));

    QQuaternion qaRb = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), this->yaw) *
                     QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), this->pitch) *
                     QQuaternion::fromAxisAndAngle(QVector3D(1,0,0), this->roll) ;
    QMatrix4x4 aRb;
    QVector3D auxVec;
    mat J(6,6);

    aRb.rotate(qaRb);
    for (int i=0; i<6; i++)
    {
        auxVec = QVector3D::crossProduct(aRb*bi.at(i) ,Si.at(i))/1000/1000;
        J(i,0) =  Si.at(i).x()/1000;
        J(i,1) =  Si.at(i).y()/1000;
        J(i,2) =  Si.at(i).z()/1000;
        J(i,3) =  auxVec.x();
        J(i,4) =  auxVec.y();
        J(i,5) =  auxVec.z();
    }


    //mat Jinv = inv(J);
    //Jinv.print("inv: ");

    //J.print("J = ");
    //vec s = svd( J );

    //qDebug()<<"det: " << det(J);
    //qDebug() <<"cond: "<< min(s)/max(s);
    //s.print("svd");



    return J;

}
