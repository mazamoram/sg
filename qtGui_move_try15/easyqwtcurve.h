#ifndef EASYQWTCURVE_H
#define EASYQWTCURVE_H
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qcolor.h>

class easyQwtCurve
{
public:
    QwtPlotCurve *curve;
    QVector<QPointF>* curveSmpls;
    QwtPlot* qwtPlotRef;
    QColor colorRef;

    easyQwtCurve();
    ~easyQwtCurve();

    void setQwtPlot(QwtPlot* qwtPlotRef);
    void setColor(QColor color);
    void set(QwtPlot* qwtPlotRef, QColor color, int lineWidth = 1, Qt::PenStyle S = Qt::SolidLine);

    void plot(QList <double> ydata);
    void updatePlot(QList <double> ydata);

    //void plot(double timeStep, QList <double> ydata);
    //void updatePlot(double timeStep, QList <double> ydata);

    void plot(QList <double> xdata, QList <double> ydata, double xbias = 0, double ybias = 0,
                                                          double xScale = 1, double yScale = 1);
    void updatePlot(QList <double> xdata, QList <double> ydata, double xbias = 0, double ybias = 0,
                                                                double xScale = 1, double yScale = 1);

    void plot(double timeStep, QList <double> ydata, double xbias = 0, double ybias = 0,
                                                          double xScale = 1, double yScale = 1);
    void updatePlot(double timeStep, QList <double> ydata, double xbias = 0, double ybias = 0,
                                                                double xScale = 1, double yScale = 1);

    void hline(double y, double ti, double tf);
    void attach();
    void dettach();
    void clear();


};

#endif // EASYQWTCURVE_H
