# README #

* This is a GUI for an industrial Stewart Gough robot.
* The interface is being develop with QtCreator under linux Ubuntu 14.04. 
* In order to avoid singularities and to keep track of the dexterity, Armadillo 
* libraries are used to compute the inverse and the singular values of the 
* jacobian matrix.
* Galil communication libraries are used to send the reference commands to a DMC  * 4183 Galil controller.


### How do I get set up? ###

* Download SG_installer.tar.gz  from https://bitbucket.org/mazamoram/sg/downloads
* Unpack, open a terminal and go to the unpacked folder.
* Concede execute permissions to the installer script using  using chmod +x install.sh
* Execute using ./install.sh and wait.
* Once installation has finished restart your computer.


### For further information please contact ###

* miguel.zamora24@gmail.com